<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * Admin Create	: mbahsomo
 * Nama File	: admin_model.php
 */
class Admin_model extends T_Model {

	private $field = array();

    function __construct() {
        parent::__construct();
        $this->set_table('admin');
        $this->set_key_field( 'u_email' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();
        
        for ($i=0; $i < count($this->field) ; $i++) { 
            $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }
    
    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function get_rule($insert = true) {
        $rl =  array(
            array(
                'field' => 'admin_email',
                'label' => 'Admin Name',
                'width' => '200',
                'rules' => 'xss_clean|max_length[50]|required'
            ),array(
                'field' => 'admin_name',
                'label' => 'Nama Depan',
                'width' => '300',
                'rules' => 'xss_clean|max_length[45]|required'
            ),array(
                'field' => 'u_password',
                'label' => 'Password',
                'grid'  => false,
                'rules' => 'xss_clean|max_length[32]'
            )
        );
        return $rl;
    }
    
    public function search($field='u_fname', $value='%', $start=0, $stop=5){
        $this->set_null();
        $this->set_fields( $this->get_table() . '.*');
        $this->set_start($start);
        $this->set_stop($stop);
        $fieldv = explode(";", $field);
        $valuev = explode(";", $value);
        if (count($valuev) > 0) {
            for ($a = 0; $a < count($valuev); $a++) {
                if ($valuev[$a] !== '') {
                    $this->set_like(array(
                        $this->get_table() . '.'.$fieldv[$a] => $valuev[$a]
                    ));
                }
            }
        }
        $this->set_params(
            array(
                'toko_email' => $this->session->admindata('admin_name'),
                'u_utama' => 'F'
            )
        );
        $this->set_orderby('date_edit desc');
        return $this->get_data();
    }
    
    public function encrypted($pass, $salt){
        return md5($pass . md5($salt));
    }

    public function get_salt($char=10){
        $kar = "0123456789BCDFGHJKLMNPQRSTVWXYZ";
        $pass = "";
        // acak karakter
        srand((double) microtime() * 1000000);
        // lakukan looping sebanyak $panjang
        for ($i = 0; $i < $char; $i++) { // default diulang sebanyak 10x
            $nom_acak = rand() % 53; // untuk mendapatkan nomor acak, pada substr()
            $pass .= substr($kar, $nom_acak, 1); // ambil satu karakter
        }

        return $pass; 
    
    }

}

/* End of file admin_model.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/sanmar/app/models/admin_model.php */