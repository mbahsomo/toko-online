<style type="text/css">
	.cls-induk{
		margin: 10px 10px 0 0;
	}
</style>
<div class="row cls-induk">
	<div class="col-md-12">
		<div class="panel panel-primary">
			  <div class="panel-heading">
					<h3 class="panel-title">Daftar Project</h3>
			  </div>
			  <div class="panel-body">
					<div style="height: 300px;" ag-grid="gridProjectOptions" class="ag-fresh"></div>
			  </div>
		</div>
	</div>
  	<div class="col-md-6">
  		<div class="panel panel-primary">
			  <div class="panel-heading">
					<h3 class="panel-title">Daftar PO</h3>
			  </div>
			  <div class="panel-body">
					<div style="height: 200px;" ag-grid="gridPOOptions" class="ag-fresh"></div>
			  </div>
		</div>
  	</div>
  	<div class="col-md-6">
  		<div class="panel panel-primary">
			  <div class="panel-heading">
					<h3 class="panel-title">Daftar Kwitansi</h3>
			  </div>
			  <div class="panel-body">
					<div style="height: 200px;" ag-grid="gridKwitansiOptions" class="ag-fresh"></div>
			  </div>
		</div>
  	</div>
  	<div class="col-md-12">
		<div class="panel panel-primary">
			  <div class="panel-heading">
					<h3 class="panel-title">Stok Barang</h3>
			  </div>
			  <div class="panel-body">
					<div style="height: 200px;" ag-grid="gridBarangOptions" class="ag-fresh"></div>
			  </div>
		</div>
	</div>
</div>