<?php
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: report_helper.php
 */
if ( ! function_exists('format_angka'))
{
	 function format_angka($val, $koma = 2, $rpt = true)
	 {
	 	if ($rpt)
	 		return number_format($val, $koma, ",","." );
	 	else
	 		return $val;
	 }
}