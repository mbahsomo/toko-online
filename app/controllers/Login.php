<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: Login.php
 */
class Login extends T_Controller {

	public function index()
	{
		$data['isi'] = $this->load->view('frontend/login',null,TRUE);
		$this->load->view('frontend/template/index',$data);
	}

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */