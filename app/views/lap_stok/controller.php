<?php
$arrfields = array();
$arrfields = set_header_aggrid ($this->mdl->get_rule(true));
?>
<script type="text/javascript">
	angular.module('DoEventApp').controllerProvider.register('<?php echo $controller; ?>', function($scope, $http, $document, toaster, $filter, $sce, $compile, doeventTools) {
		$scope.barangs = [];
		$http({
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            url: '<?php echo site_url( 'product/get_all/' . $this->session->userdata('hashkey')) ; ?>',
            method: "POST"
        }).success(function(data) {
            if (data !== undefined) {
                $scope.barangs = data.rec;
            }
        });
	});
</script>	