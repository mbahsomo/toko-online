<script type="text/javascript">
	angular.module('DoEventApp').controllerProvider.register('<?php echo $controller; ?>', function($scope, $http, $document, toaster, $filter, $sce, $compile) {

		/*Daftar Project*/
		var columnProjectDefs = [
	        {headerName: "#", field: "p_code", width: 60, cellRenderer : projectDetail},
	        {headerName: "TGL", field: "p_start", width: 80},
	        {headerName: "KODE", field: "p_code", width: 120},
	        {headerName: "NAMA", field: "p_name", width: 200},
	    ];
		$scope.gridProjectOptions = {
	        columnDefs: columnProjectDefs,
	        angularCompileRows: true,
	        rowData: null,
	        rowSelection: 'single',
	        enableColResize: true,
	        enableSorting: true,
	        enableFilter: true,
	    };
	    $scope.loadGridProjects = function(){
	    	$http({
	            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	            url: '<?php echo site_url('awal/ambil_project/' . $this->session->userdata('hashkey')) ; ?>',
	            method: "POST"
	        }).success(function(data) {
	            if (data !== undefined) {
	                /*$scope.gridProjectOptions.rowData = data.rec;*/
		        	$scope.gridProjectOptions.api.setRows(data.rec);
	            }
	        });
	    };
	    $scope.loadGridProjects();

	    function projectClicked(params){
	    	alert('test');
	    }

	    function projectDetail(params) {
	        /*params.$scope.projectClicked = projectClicked;*/
	        return '<div align="center"><a href="<?php echo site_url();?>#/awal/detail_project/{{data.p_code}}" class="btn btn-success btn-xs" ng-click="projectClicked(data.p_code)" >Detail</a><div>';
	    }

	    /*Daftar PO*/
		$scope.gridPOOptions = {
	        columnDefs: columnProjectDefs,
	        angularCompileRows: true,
	        rowData: null,
	        rowSelection: 'single',
	        enableColResize: true,
	        enableSorting: true,
	        enableFilter: true,
	    };

	    /*Daftar PO*/
		$scope.gridKwitansiOptions = {
	        columnDefs: columnProjectDefs,
	        angularCompileRows: true,
	        rowData: null,
	        rowSelection: 'single',
	        enableColResize: true,
	        enableSorting: true,
	        enableFilter: true,
	    };

	    /*Daftar PO*/
		$scope.gridBarangOptions = {
	        columnDefs: columnProjectDefs,
	        angularCompileRows: true,
	        rowData: null,
	        rowSelection: 'single',
	        enableColResize: true,
	        enableSorting: true,
	        enableFilter: true,
	    };

	});
</script>	