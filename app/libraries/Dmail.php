<?php

/*
 * ** Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * Description of dmail
 *
 * @author mbahsomo
 * Contoh Penggunaan
 * $data = array(
 *      "subject"    => "[more2buy] Pemesanan Iklan"
 *      ,"message"    => $pesan2
 *      ,"tujuan"     => "angga@more2buy.com"
 *      ,"from"       => $this->emailmaster
 * );
 * $this->dmail->sendmail($data);
 */
class Dmail {
    var $mail = array();

    public function __construct() {
        $this->CI = & get_instance();
        $this->mailhost = 'ssl://smtp.googlemail.com';//$this->CI->config->item('email_host');
        $this->mailport = '465';//$this->CI->config->item('email_port');
        $this->mailmaster = 'noreply@do-event.com';//$this->CI->config->item('email_master');
        $this->mailmasterpw = 'admin@noreply';//$this->CI->config->item('email_master-password');
        $this->webtitle = 'T-Market';//$this->CI->config->item('title');
        $this->noreply  = 'noreply@do-event.com';//$this->CI->config->item('email_master');
    }

    /*
     * data yang dikirim ($data) adalah Array
     *
     * array("subject"  => "subject"
     *      , "message"    => "isi pesannya"
     *      , "tujuan" => webmaster@more2buy.com
     * )
     */
    function sendMail($data){
       $config = Array(
           'protocol'  => 'smtp',
           'smtp_host' => $this->mailhost,
           'smtp_port' => $this->mailport,
           'smtp_timeout' => "7",
           'smtp_user' => $this->mailmaster,
           'smtp_pass' => $this->mailmasterpw,
           'newline'   => "\r\n",
           'useragent' => $this->webtitle,
           'mailtype'  => 'html',
           'wordwrap'  => TRUE
       );

        /*$config = Array(
            'protocol'  => 'sendmail',
            'newline'   => "\r\n",
            'useragent' => $this->webtitle,
            'mailtype'  => 'html',
            'wordwrap'  => TRUE
        );*/
        $this->CI->load->library('email', $config);

        $from = ($data['from'] == "")?$this->noreply:$data['from'];

        if(is_array($data['tujuan'])):
            foreach ($data['tujuan'] as $tujuan):                

                $this->CI->email->from($from);
                $this->CI->email->to($tujuan);
                $this->CI->email->subject($data['subject']);
                $this->CI->email->message($data['message']);
                if($data['attach'] <> ""):
                    if(is_array($data['attach'])){
                        foreach ($data['attach'] as $att):
                            $this->CI->email->attach($att);
                        endforeach;
                    }else{
                        $this->CI->email->attach($data['attach']);
                    }
                endif;
                $send = $this->CI->email->send();
                unset ($data['attach']);
                $this->CI->email->clear(TRUE);
            endforeach;
        else:
            $this->CI->email->from($from);
            $this->CI->email->to($data["tujuan"]);
            $this->CI->email->subject($data['subject']);
            $this->CI->email->message($data['message']);            
            if($data['attach'] <> ""):
                if(is_array($data['attach'])){
                    foreach ($data['attach'] as $att):
                        $this->CI->email->attach($att);
                    endforeach;
                }else{
                    $this->CI->email->attach($data['attach']);
                }
            endif;
            $send = $this->CI->email->send();
            unset ($data['attach']);
            $this->CI->email->clear(TRUE);
        endif;
        
        return $send;
    }
}
