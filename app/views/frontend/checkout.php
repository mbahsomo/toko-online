<script src="<?php echo base_url('assets/lib/angularjs/angular.min.js'); ?>"></script>
<script type="text/javascript">
	function CartCtrl($scope, $http){
		$scope.rec = [] ;
		$scope.propinsis = [];
		$scope.kotas = [];
		$scope.baiayKirim = 0;
		$scope.user = {};

		$http({
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			url: '<?php echo site_url('shipping_price/get_propinsi') ; ?>',
			method: "POST"
		}).success(function(data) {
			if (data !== undefined) {
				$scope.propinsis = data.rec;
				//console.log($scope.propinsi);
			}
		});

		$http({
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			url: '<?php echo site_url('shipping_price/get_kota') ; ?>',
			method: "POST"
		}).success(function(data) {
			if (data !== undefined) {
				$scope.kotas = data.rec;
			}
		});

		$scope.LoadCart = function(){
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('cart/getcart') ; ?>',
				method: "POST"				
			}).success(function(data) {
				if (data !== undefined) {
					$scope.rec = data.rec;
				}
			});
		}
		$scope.LoadCart();

		$scope.LoadBiayaKirim = function(){
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('checkout/getbiayakirim') ; ?>',
				method: "POST"				
			}).success(function(data) {
				if (data !== undefined) {
					$scope.baiayKirim = data.biaya;
					$scope.user = data.user;
				}
			});
		}
		$scope.LoadBiayaKirim();

		$scope.EditQty = function(items){
			var vdata = {
				rowid : items.rowid,
				qty : items.qty
			};
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('cart/updatecart') ; ?>',
				method: "POST",
				data: $.param(vdata)				
			}).success(function(data) {
				if (data !== undefined) {
					if(data.success==false){
						alert('Stok tinggal ' + data.qty);
						items.qty = data.qty;			
					}
				}
			});
		}

		$scope.Remove = function(items, idx){
			var vdata = {
				rowid : items.rowid,
				qty : 0
			};
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('cart/updatecart') ; ?>',
				method: "POST",
				data: $.param(vdata)				
			}).success(function(data) {
				if (data !== undefined) {
					$scope.LoadCart();
				}
			});
		};
		
		$scope.GetTotal = function(){
			var total = 0 ;
			angular.forEach($scope.rec, function(value, key){
				total += (value.price * value.qty);
			});
			return total
		};

		$scope.GrandTotal = function(a, b){
			return eval(a + "+" + b);
		}

		$scope.SimpanTransaksi = function(){
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('checkout/simpan') ; ?>',
				method: "POST"
			}).success(function(data) {
				if (data !== undefined) {
					if(data.success==true){
						window.location.href = data.url;
					}
				}
			});
		}

		$scope.UpdateUser = function(){
            $http({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: '<?php echo site_url( 'profile/update_user/' . $this->session->userdata('hashkey')) ; ?>',
                method: "POST",
                data: $.param($scope.user)
            }).success(function(data) {
                if (data !== undefined) {
                    if(data.success==true){
                        alert('Data Telah Tersimpan');
                        $scope.LoadBiayaKirim();
                    }
                }
            });
        }

	};	
</script>

<section id="cart_items" ng-app>
	<div class="container" ng-controller="CartCtrl">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
			  <li><a href="#">Home</a></li>
			  <li class="active">Check out</li>
			</ol>
		</div><!--/breadcrums-->

		<div class="step-one">
			<h2 class="heading">Step1</h2>
		</div>
		
		<div class="shopper-informations">
			<div class="row">
				<div class="col-sm-6">
					<div class="shopper-info">
						<p>Informasi penerima barang</p>
						<form>
							<input readonly type="text"  ng-model="user.user_email" >
							<input readonly type="text"  ng-model="user.user_name" >
							<input type="text"  ng-model="user.user_address" >
							<div>
								<select required ng-model="user.user_state">
									<option>Pilih Propinsi</option>
									<option ng-repeat="pr in propinsis" value="{{pr.price_state_name}}">{{pr.price_state_name}}</option>
								</select>
							</div>
							<div style="margin : 10px 0 10px 0;">
								<select required ng-model="user.user_city">
									<option>Pilih Kota</option>
									<option ng-repeat="kt in kotas | filter:user.user_state" value="{{kt.price_city_name}}">{{kt.price_city_name}}</option>
								</select><br>
							</div>
						</form>
						<a class="btn btn-primary" href="" ng-click="UpdateUser()">Update Alamat</a>
						<a class="btn btn-primary" href="<?php echo site_url(); ?>">Tambah Barang</a>
						<a class="btn btn-primary" href="" ng-click="SimpanTransaksi()" >Lanjutkan Bayar</a>
					</div>
				</div>
				<div class="col-sm-5 clearfix">
					
				</div>
				<div class="col-sm-4">
						
				</div>					
			</div>
		</div>
		<div class="review-payment">
			<h2>Review & Payment</h2>
		</div>

		<div class="table-responsive cart_info">
			<table class="table table-condensed">
				<thead>
					<tr class="cart_menu">
						<td class="image">ID Barang</td>
						<td class="description"></td>
						<td class="price">Price</td>
						<td class="quantity">Quantity</td>
						<td class="total">Total</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="items in rec">
						<td class="cart_product">
							#{{items.id}}
						</td>
						<td class="cart_description">
							{{items.name}}
						</td>
						<td class="cart_price">
							<p>Rp. {{items.price | number}}</p>
						</td>
						<td class="cart_quantity">
							<div class="cart_quantity_button">
								<input class="cart_quantity_input" ng-blur="EditQty(items)" type="text" name="quantity" ng-model="items.qty" autocomplete="off" size="4">
							</div>
						</td>
						<td class="cart_total">
							<p class="cart_total_price">Rp. {{items.price*items.qty | number}}</p>
						</td>
						<td class="cart_delete">
							<a ng-click="Remove(items, $index)" class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan="4">&nbsp;</td>
						<td colspan="2">
							<table class="table table-condensed total-result">
								<tr>
									<td>Sub Total</td>
									<td><p class="cart_total_price">Rp. {{ GetTotal() | number}}</p></td>
								</tr>
								<tr class="shipping-cost">
									<td>Biaya Kirim</td>
									<td><p class="cart_total_price">Rp. {{ baiayKirim | number}}</p></td>										
								</tr>
								<tr>
									<td>Total</td>
									<td><p class="cart_total_price">Rp. {{ GrandTotal(baiayKirim, GetTotal()) | number}}</p></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="payment-options">
				<span>
					<label><input type="checkbox"> Direct Bank Transfer</label>
				</span>
			</div>
	</div>
</section> <!--/#cart_items-->