<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Online Shop</title>
    <link href="<?php echo base_url('assets/theme/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/css/prettyPhoto.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/css/price-range.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/css/animate.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/theme/css/main.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/theme/css/responsive.css'); ?>" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/theme/images/ico/apple-touch-icon-144-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/theme/images/ico/apple-touch-icon-114-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/theme/images/ico/apple-touch-icon-72-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/theme/images/ico/apple-touch-icon-57-precomposed.png'); ?>">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="<?php echo site_url('cart');?>"><i class="fa fa-envelope"></i>Chart</a></li>
								<?php  if($this->session->userdata('login')===1){ ?>
								<li><a href="<?php echo site_url('toko/logout');?>"><i class="fa fa-envelope"></i>Log Out</a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="<?php echo site_url();?>" <?php echo ($this->cid=='toko')?'class="active"':''; ?> >Beranda</a></li>
								<li class="dropdown"><a href="#" <?php echo ($this->cid=='chart')?'class="active"':''; ?> <?php echo ($this->cid=='checkout')?'class="active"':''; ?> <?php echo ($this->cid=='login')?'class="active"':''; ?>>Toko<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="<?php echo site_url('checkout');?>">Checkout</a></li> 
										<li><a href="<?php echo site_url('cart');?>">Kerangjang Belanja</a></li>
                                    </ul>
                                </li> 
                                <li><a href="<?php echo site_url('produk_terbaru');?>" <?php echo ($this->cid=='produk_terbaru')?'class="active"':''; ?>>Produk Terbaru</a></li>
                                <li><a href="<?php echo site_url('produk_terlaris');?>" <?php echo ($this->cid=='produk_terlaris')?'class="active"':''; ?>>Produk Terlaris</a></li>
                                
                                <?php if($this->session->userdata('login')!==1){ ?>
                                <li><a href="<?php echo site_url('login');?>">Daftar / Login </a></li>
                                <?php } ?>
                                <?php if($this->session->userdata('login')){ ?>
                                <li class="dropdown"><a href="#">Account<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="<?php echo site_url('account/transaksi');?>">Daftar Transaksi</a></li> 
										<li><a href="<?php echo site_url('account/pembayaran');?>">Pembayaran Transaksi</a></li>
										<li><a href="<?php echo site_url('account');?>">Edit User</a></li>
                                    </ul>
                                </li>
                                <?php } ?>
								<li><a href="<?php echo site_url('about');?>">Tentang Kami</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	<script src="<?php echo base_url('assets/theme/js/jquery.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/js/jquery.scrollUp.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/js/price-range.js'); ?>"></script>
    <script src="<?php echo base_url('assets/theme/js/jquery.prettyPhoto.js'); ?>"></script>
    <script src="<?php echo base_url('assets/theme/js/main.js'); ?>"></script>
    
	<!-- Isi -->
	<?php echo $isi; ?>
	
	<footer id="footer"><!--Footer-->
			
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2013 E-SHOPPER Inc. All rights reserved.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
    
</body>
</html>