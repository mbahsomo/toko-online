<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * Kasus Create : mbahsomo
 * Nama File    : Order_model.php
 */
class Order_model extends T_Model {

    private $field = array();

    function __construct() {
        parent::__construct();
        $this->set_table('order');
        $this->set_key_field( 'order_id' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();
        
        for ($i=0; $i < count($this->field) ; $i++) { 
            $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        /*$fields['date_edit'] = date('Y-m-d H:i:s');
        $fields['user_entry'] = $this->session->userdata('user_name');*/
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        /*$fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);*/
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }
    
    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->delete_data();
    }

    public function get_rule($insert = true) {
        $rl =  array(
            array(
                'field' => 'user_email',
                'label' => 'User',
                'width' => 200,
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'order_date',
                'label' => 'Tanggal',
                'width' => 200,
                'rules' => 'xss_clean|max_length[25]|required'
            ),array(
                'field' => 'order_note',
                'label' => 'Catatan',
                'width' => 200,
                'rules' => 'xss_clean'
            ),array(
                'field' => 'order_total',
                'label' => 'Total',
                'width' => 200,
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'order_shipprice',
                'label' => 'Price',
                'width' => 200,
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'order_status',
                'label' => 'Status',
                'width' => 200,
                'rules' => 'xss_clean|numeric|required'
            )
        );
        if (!$insert) {
            return array_merge(
                array(
                    array(
                        'field' => 'order_id',
                        'label' => 'ID',
                        'rules' => 'required|numeric|required'
                    )
                ), $rl
            );
        } else {
            return $rl;
        }
    }
    
    public function search($field='u_fname', $value='%', $start=0, $stop=5){
        $this->_check_order();
        $this->set_null();
        $this->set_fields( $this->get_table() . '.*');
        $this->set_start($start);
        $this->set_stop($stop);
        $fieldv = explode(";", $field);
        $valuev = explode(";", $value);
        if (count($valuev) > 0) {
            for ($a = 0; $a < count($valuev); $a++) {
                if ($valuev[$a] !== '') {
                    $this->set_like(array(
                        $this->get_table() . '.'.$fieldv[$a] => $valuev[$a]
                    ));
                }
            }
        }
        //$this->set_orderby('date_edit desc');
        return $this->get_data();
    }
        
    private function _check_order()
    {
        $this->set_null();
        $data = $this->exec_query("select * from `order` where order_status='O'",true);
        foreach ($data as $key => $value) {
            if ((strtotime(date('Y-m-d H:i:s' )) - strtotime( $value['order_date'] )) / (60*24*60) > 1){
                $this->_update_stok($value['order_id']);
                $this->set_null();
                $this->exec_query("update `order` set order_status='C' where order_id=" . $value['order_id'], false);
            }
        }
    }

    private function _update_stok($id)
    {
        $this->load->model('Order_detail_model');
        $this->Order_detail_model->set_null();
        $this->Order_detail_model->set_fields('product_id, orderd_qty');
        $this->Order_detail_model->set_params(array('order_id'=>$id));
        foreach ($this->Order_detail_model->get_data() as $key => $value) {
            $this->Order_detail_model->set_null();
            $this->Order_detail_model->exec_query("update product set product_qty = product_qty+" . $value['orderd_qty'] ." where product_id=" . $value['product_id'], false);
        }
    }
}

/* End of file Order_model.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/sanmar/app/models/Order_model.php */