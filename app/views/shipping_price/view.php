<div class="toolbarmain">
<?php $this->load->view('template/toolbar'); ?>
</div>
<div class="gridStyleAg">
    <div style="height: 100%;" ag-grid="gridOptions" class="ag-fresh"></div>    
</div>
<div class="paging-fo">
    <paging
        class="small"
        page="currentPage" 
        page-size="pageSize" 
        total="ttlRec" 
        adjacent="2"
        dots=".."
        scroll-top="false" 
        hide-if-empty="true"
        ul-class="pagination"
        active-class="active"
        disabled-class="disabled"
        show-prev-next="true"
        paging-action="DoCtrlPagingAct('Paging Clicked', page, pageSize, total)">
    </paging>
    <div class="total">Total Record : {{ttlRec}}</div>
</div>
<!--Form -->
<div class="modal fade bootstrap-dialog type-primary" id="window-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"> Add / Edit data</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" id="frmProgram" ng-submit="Save(dataRecSelect[0])">
                    <div class="form-group form-group-sm" >
                        <label for="price_state_name" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('price_state_name'); ?></label>
                        <div class="col-sm-9">
                            <input de-focus maxlength="50" required type="text" class="form-control input-sm" id="price_state_name" ng-model="dataRecSelect[0].price_state_name" placeholder="<?php echo $this->mdl->get_label('price_state_name'); ?>">
                        </div>
                    </div>
                    <div class="form-group form-group-sm" >
                        <label for="price_city_name" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('price_city_name'); ?></label>
                        <div class="col-sm-9">
                            <input de-focus maxlength="50" required type="text" class="form-control input-sm" id="price_city_name" ng-model="dataRecSelect[0].price_city_name" placeholder="<?php echo $this->mdl->get_label('price_city_name'); ?>">
                        </div>
                    </div>
                    <div class="form-group form-group-sm" >
                        <label for="price_harga" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('price_harga'); ?></label>
                        <div class="col-sm-3">
                            <input de-focus format="number" required type="text" class="form-control input-sm clstdnumber" id="price_harga" ng-model="dataRecSelect[0].price_harga" placeholder="<?php echo $this->mdl->get_label('price_harga'); ?>">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmProgram" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-floppy-disk"></span> {{setlang.Translet('save')}}</button>
                <button type="button" ng-click="CancelSave()" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-folder-open"></span> {{setlang.Translet('close')}}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Filter Form -->
<div class="modal fade bootstrap-dialog type-primary" id="window-filter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="glyphicon glyphicon-filter"></i> Filter</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group form-group-sm" ng-repeat="(key, value) in gridOptions.columnDefs" >
                        <label for="input{{value.field}}" class="col-sm-3 control-label">{{value.headerName}}</label>
                        <div class="col-sm-9">
                            <input de-focus type="text" class="form-control input-sm" id="input{{value.field}}" placeholder="{{value.headerName}}" ng-model="fields[value.field]">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" ng-click="FilterData()" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-filter"></i> {{setlang.Translet('filter')}}</button>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> {{setlang.Translet('close')}}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<toaster-container toaster-options="{'time-out': 1000, 'close-button':true,'position-class':'toast-top-center'}"></toaster-container>
<script type="text/javascript">
    $(".modal").draggable({
        handle: ".modal-header"
    });
</script>