<?php 
//echo  (strtotime(date('Y-m-d H:i:s' , strtotime('2015-08-27 00:00:12'))) - strtotime('2015-08-26 00:00:12')) / (60*24*60) ;
?>
<script src="<?php echo base_url('assets/lib/angularjs/angular.min.js'); ?>"></script>
<script type="text/javascript">
	function TransaksiCtrl($scope, $http){
		$scope.recMaster = [];
		$scoperecDetail = [] ;
		$scope.loadMaster = function(){
			var vparams = {
                'field': 'user_email' ,
                'value': '<?php echo $this->session->userdata('name'); ?>' ,
                'limit': 10000,
                'stop' : 0
            };
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('order/search') ; ?>',
				method: "POST",
				data: $.param(vparams)
			}).success(function(data) {
				if (data !== undefined) {
					$scope.recMaster = data.rec;
				}
			});
		}
		$scope.loadMaster();
		$scope.loadDetail = function(id){
			var vparams = {
                'kode': id
            };
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('order/getdetail') ; ?>',
				method: "POST",
				data: $.param(vparams)
			}).success(function(data) {
				if (data !== undefined) {
					$scope.recDetail = data.rec;
				}
			});
		}

		$scope.Batal = function(items){
			if (confirm('Apakah benar mau membatalkan transaksi ini ...!!!')) {
			    if (items.order_status!= 'P' ){
			    	$http({
						headers: {'Content-Type': 'application/x-www-form-urlencoded'},
						url: '<?php echo site_url('order/batal') ; ?>',
						method: "POST",
						data: $.param({'kode' : items.order_id})
					}).success(function(data) {
						if (data !== undefined) {
							if(data.success==true)
								items.order_status = 'C';
						}
					});
			    }else{
			    	alert('Maaf status barang bukan order...!');
			    }
			}
		}

		$scope.getStatus = function (sts){
			console.log(sts);
			if (sts=='O'){
				return 'Order';
			}else if (sts=='C'){
				return 'Batal';
			}if (sts=='P'){
				return 'Bayar';
			}
		}
	};	
</script>
<section ng-app>
	<div class="container" ng-controller="TransaksiCtrl">
		<div class="row">
			<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Id</th>
								<th>Tgl</th>
								<th>Status</th>
								<th>Detail</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="items in recMaster">
								<td>#{{items.order_id}}</td>
								<td>{{items.order_date}}</td>
								<td>{{getStatus(items.order_status)}}</td>
								<td>
									<button type="button" class="btn btn-danger btn-xs" ng-click="Batal(items)">Batal</button>
									<button type="button" class="btn btn-success btn-xs" ng-click="loadDetail(items.order_id)">Detail</button>
								</td>
							</tr>
						</tbody>
					</table>	
				</div>
			</div>

			<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
				<div class="table-responsive">		
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Id</th>
								<th>Barang</th>
								<th>Qty</th>
								<th>Harga</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="items in recDetail">
								<td>{{items.product_id}}</td>
								<td>{{items.product_name}}</td>
								<td>{{items.orderd_qty}}</td>
								<td>{{items.orderd_total}}</td>
								<td>{{items.orderd_total * items.orderd_qty}}</td>
							</tr>
						</tbody>
					</table>	
				</div>
			</div>
		</div>		
	</div>
</section>