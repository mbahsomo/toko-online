<script src="<?php echo base_url('assets/lib/angularjs/angular.min.js'); ?>"></script>
<script type="text/javascript">
	function BayarCtrl($scope, $http){
		$scope.bayar = {
			order_id : 0,
			payment_date : '<?php echo date('Y-m-d'); ?>',
			payment_bankfrom :'',
			payment_bankto : '',
			payment_bankaccount : '',
			payment_transfer : 0,
			paymenyt_transferdate : '<?php echo date('Y-m-d'); ?>'
		};
		$scope.noorder = [];
		$scope.loadNomor = function(){
            $http({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: '<?php echo site_url( 'order/get_order/' . $this->session->userdata('hashkey')) ; ?>',
                method: "POST",
                
            }).success(function(data) {
                if (data !== undefined) {
                	$scope.noorder=data.rec;
                }
            });
        }
        $scope.loadNomor();
		$scope.Save = function(){
            $http({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: '<?php echo site_url( 'payment/simpan_manual/' . $this->session->userdata('hashkey')) ; ?>',
                method: "POST",
                data: $.param($scope.bayar)
            }).success(function(data) {
                if (data !== undefined) {
                    if(data.success==true)
                        alert('Data Telah Tersimpan');
                }
            });
        }

        $scope.$watch('bayar.order_id', function (newVal, oldVal) {
	        if (newVal !== oldVal ) {
	        	$http({
	                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	                url: '<?php echo site_url( 'order/get_harga/' . $this->session->userdata('hashkey')) ; ?>',
	                method: "POST",
	                data: $.param({order_id : newVal})
	            }).success(function(data) {
	                if (data !== undefined) {
	                    console.log(data.rec[0].order_total);
	                    $scope.bayar.payment_transfer = parseFloat(data.rec[0].order_total) + parseFloat(data.rec[0].order_shipprice);
	                }
	            });
	        }
	    }, true);

	}
	
</script>
<section id="cart_items" ng-app>
	<div class="container" ng-controller="BayarCtrl">
	
		<div class="shopper-informations">
			<div class="row">
				<div class="col-sm-6">
					<div class="shopper-info">
						<p>Pembayaran</p>
						<form class="form-horizontal" role="form" id="frmProgram" ng-submit="Save(dataRecSelect[0])">
		                    <div class="form-group form-group-sm" >
		                        <label for="order_id" class="col-sm-3 control-label">No Nota</label>
		                        <div class="col-sm-9">
		                            <select ng-model="bayar.order_id" class="form-control input-sm">
		                            	<option ng-repeat="jnb in noorder" value="{{jnb.order_id}}">{{jnb.order_id}}</option>
		                            </select>
		                        </div>
		                    </div>
		                    <div class="form-group form-group-sm" >
		                        <label for="category_name" class="col-sm-3 control-label">Bank Asal</label>
		                        <div class="col-sm-9">
		                            <input de-focus maxlength="50" required type="text" class="form-control input-sm" ng-model="bayar.payment_bankfrom" placeholder="Bank Asal">
		                        </div>
		                    </div>
		                    <div class="form-group form-group-sm" >
		                        <label for="category_name" class="col-sm-3 control-label">Bank Tujuan</label>
		                        <div class="col-sm-9">
		                            <input de-focus maxlength="50" required type="text" class="form-control input-sm" ng-model="bayar.payment_bankto" placeholder="Bank Tujuan">
		                        </div>
		                    </div> 
		                    <div class="form-group form-group-sm" >
		                        <label for="category_name" class="col-sm-3 control-label">Account</label>
		                        <div class="col-sm-9">
		                            <input de-focus maxlength="50" required type="text" class="form-control input-sm" ng-model="bayar.payment_bankaccount" placeholder="Account">
		                        </div>
		                    </div>               
		                    <div class="form-group form-group-sm" >
		                        <label for="category_name" class="col-sm-3 control-label">Transfer</label>
		                        <div class="col-sm-9">
		                            <input de-focus format="number" maxlength="50" required type="text" class="form-control input-sm" ng-model="bayar.payment_transfer" placeholder="Transfer">
		                        </div>
		                    </div>
		                </form>
						<!-- <button type="submit" form="frmSimpan" class="btn btn-primary">Kirim Bayar</button> -->
						<a class="btn btn-primary" href="<?php echo site_url(''); ?>" ng-click="Save()">Kirim Bayar</a>
					</div>
				</div>
								
			</div>
		</div>
	</div>
</section>