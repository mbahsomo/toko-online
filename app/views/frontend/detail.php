<script src="<?php echo base_url('assets/lib/angularjs/angular.min.js'); ?>"></script>
<script type="text/javascript">
	function BeliCtrl($scope, $http){
		$scope.qty = <?php echo $barang[0]['product_qty']; ?>;
		$scope.IsiCart = function(id){
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('cart/add2cart') ; ?>',
				method: "POST",
				data: $.param( {kode:id,qty: $scope.qty} )
			}).success(function(data) {
				if (data !== undefined) {
					if(data.success==false){
						alert(data.msg);
					}else{
						alert ('Sudah di tambahkan ke keranjang belanja');	
					}
					
				}
			});
		}
	};	
</script>
<section ng-app>
	<div class="container" ng-controller="BeliCtrl">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
					<h2>Category</h2>
					<div class="panel-group category-products" id="accordian">
						<?php foreach($cat as $key => $value) {?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="<?php echo site_url('toko/kategory/' . $value['category_id'] .'/' .$value['category_name']); ?>"><?php echo $value['category_name'] ; ?></a></h4>									
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="col-sm-9 padding-right">
				<?php 
				foreach ($barang as $key => $value) { 

					$satu = $value['images'];
				?>
				<div class="product-details"><!--product-details-->
					<div class="col-sm-5">
						<div class="view-product">
							<img id="img-detail" src="<?php echo base_url('assets/images/barang/' . $satu[0]['pi_image'] );?>" alt="" />
							<h3>ZOOM</h3>
						</div>
						<div id="similar-product" class="carousel slide" data-ride="carousel">
						  <!-- Wrapper for slides -->
						    <div class="carousel-inner">
								<div class="item active">
									<?php 
									$urut = 1 ;
									foreach ($value['images'] as $rowsd) {
										if($urut>1 && $urut%3==0){
											echo '</div><div class="item">';
										}
									?>
								  	<img src="<?php echo base_url('assets/images/barang/thumb/' . $rowsd['pi_image'] );?>" alt="<?php echo $rowsd['pi_image']; ?>">
								  	<?php $urut++; 
								  	} 
										/*if($urut>3){
											echo "</div>"
										}*/								  	
								  	?>
								</div>
							</div>

							<!-- Controls -->
							<a class="left item-control" href="#similar-product" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							</a>
							<a class="right item-control" href="#similar-product" data-slide="next">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>

					</div>

					<div class="col-sm-7">
						<div class="product-information"><!--/product-information-->
							<img src="<?php echo base_url('assets/theme/images/product-details/new.jpg');?>" class="newarrival" alt="" />
							<h2><?php echo $value['product_name']; ?></h2>
							<p>Web ID: #<?php echo $value['product_id']; ?></p>
							<span>
								<span>Rp <?php echo number_format($value['product_price']) ; ?></span>
								<label>Jumlah :</label>
								<input ng-model="qty" type="text" value="<?php echo $value['product_qty']; ?>" />
								<button ng-click="IsiCart(<?php echo $value['product_id']; ?>)" type="button" class="btn btn-fefault cart">
									<i class="fa fa-shopping-cart"></i>
									Add to cart
								</button>
							</span>
							<p>Stok = <?php echo $value['product_qty']; ?></p>
							<p>Keterangan : </p>
							<p><?php echo $value['product_desc']; ?></p>
							<!-- <p><b>Availability:</b> In Stock</p>
							<p><b>Condition:</b> New</p>
							<p><b>Brand:</b> E-SHOPPER</p> -->
							
						</div><!--/product-information-->
					</div>
				</div><!--/product-details-->
				<?php } ?>
				
				<script type="text/javascript">
					$(function () {
						$('.item img').click(function (e) {
							//$('#img-detail').attr('src', '<?php echo base_url('assets/images/barang');?>/' + e.currentTarget.alt);
							$('#img-detail').attr({
								src:'<?php echo base_url('assets/images/barang');?>/' + e.currentTarget.alt
							});
							console.log($('#img-detail').attr('src'));
							e.preventDefault();
						});
					});
				</script>
			</div>
		</div>
	</div>
</section>