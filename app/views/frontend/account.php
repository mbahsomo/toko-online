<script src="<?php echo base_url('assets/lib/angularjs/angular.min.js'); ?>"></script>
<script type="text/javascript">
	function UserCtrl($scope, $http){
		$scope.propinsis = [];
		$scope.kotas = [];

		$http({
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			url: '<?php echo site_url('shipping_price/get_propinsi') ; ?>',
			method: "POST"
		}).success(function(data) {
			if (data !== undefined) {
				$scope.propinsis = data.rec;
				console.log($scope.propinsi);
			}
		});

		$http({
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			url: '<?php echo site_url('shipping_price/get_kota') ; ?>',
			method: "POST"
		}).success(function(data) {
			if (data !== undefined) {
				$scope.kotas = data.rec;
			}
		});

		$scope.LoadUser = function(){
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('profile/get_user') ; ?>',
				method: "POST"				
			}).success(function(data) {
				if (data !== undefined) {
					$scope.user = data.user;
					$scope.user.user_password = "";
				}
			});
		}
		$scope.LoadUser();
		$scope.UpdateUser = function(){
            $http({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: '<?php echo site_url( 'profile/update_user/' . $this->session->userdata('hashkey')) ; ?>',
                method: "POST",
                data: $.param($scope.user)
            }).success(function(data) {
                if (data !== undefined) {
                    if(data.success==true)
                        alert('Data Telah Tersimpan');
                }
            });
        }
	}
</script>
<section id="cart_items" ng-app>
	<div class="container" ng-controller="UserCtrl">
	
		<div class="shopper-informations">
			<div class="row">
				<div class="col-sm-6">
					<div class="shopper-info">
						<p>Ubah User</p>
						<form>
							<input readonly type="text"  ng-model="user.user_email" >
							<input type="text"  ng-model="user.user_name" placeholder="Username">
							<input type="password"  ng-model="user.user_password" placeholder="Password">
							<div>
								<select required ng-model="user.user_state">
									<option>Pilih Propinsi</option>
									<option ng-repeat="pr in propinsis" value="{{pr.price_state_name}}">{{pr.price_state_name}}</option>
								</select>

							</div>
							<div style="margin : 10px 0 10px 0;">
								<select required ng-model="user.user_city">
									<option>Pilih Kota</option>
									<option ng-repeat="kt in kotas | filter:user.user_state" value="{{kt.price_city_name}}">{{kt.price_city_name}}</option>
								</select><br>
							</div>
							<input type="text"  ng-model="user.user_address" placeholder="Alamat">
						</form>
						<a class="btn btn-primary" href="" ng-click="UpdateUser()" >Simpan</a>
					</div>
				</div>
								
			</div>
		</div>
	</div>
</section>