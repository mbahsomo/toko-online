<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Detail extends CI_Controller {

	public function index($id,$name="")
	{
		$this->load->model('Product_categori_model');
		$this->load->model('Product_model');
		$data1['cat'] = $this->Product_categori_model->get_all();
		$data1['barang'] = $this->Product_model->detail($id);
		$data2['isi'] = $this->load->view('frontend/detail',$data1,true);
		$this->load->view('frontend/template/index',$data2);
	}

}

/* End of file detail.php */
/* Location: ./application/controllers/detail.php */