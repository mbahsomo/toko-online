<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends T_Controller {

	function __construct(){
        parent::__construct();
	}

	public function index()
	{
		$data['isi'] = $this->load->view('frontend/cart',null,true);
		$this->load->view('frontend/template/index',$data);
	}

	public function add2cart(){
		$qty = ($this->input->post('qty',true)>0)?$this->input->post('qty',true):1;
		$this->load->model('Product_model', 'mdl');
		$id = $this->input->post('kode',true);
		$this->mdl->set_null();
		$this->mdl->set_fields('*');
		$this->mdl->set_params(array('product_id'=>$id));
		$barang = $this->mdl->get_data();
		$data = array(
		        'id'      => $id,
		        'qty'     => $qty,
		        'price'   => $barang[0]['product_price'],
		        'name'    => $barang[0]['product_name']
		);
		$benar = false;
		$msg = '';
		if($barang[0]['product_qty']<$qty){
			$benar = false;
			$msg = 'Maaf stok hanya tersisa ' . $barang[0]['product_qty'];
		}else{
			$benar = true;
			$this->cart->insert($data);
			$msg = '';
		}
		$this->output
            ->set_content_type('application/json')
            ->set_output(
                json_encode(
                    array(
                    	'success' => $benar,
                    	'msg' => $msg
                	)                    
                )
            );
	}

	public function updatecart(){
		$item  = $this->cart->get_item($this->input->post('rowid',true));
		$this->load->model('Product_model', 'mdl');
		$id = $this->input->post('kode',true);
		$this->mdl->set_null();
		$this->mdl->set_fields('*');
		$this->mdl->set_params(array('product_id'=>$item['id']));
		$barang = $this->mdl->get_data();
		$sukes = false;
		$jml = 0 ;
		if(  $barang[0]['product_qty']< $this->input->post('qty',true) ){
			$sukes = false;
			$jml = $barang[0]['product_qty'];
		}else{
			$data = array(
		        'rowid' => $this->input->post('rowid',true),
		        'qty'   => $this->input->post('qty',true)
			);
			$sukes = $this->cart->update($data);
			$this->input->post('qty',true);
		}
		
		$this->output
            ->set_content_type('application/json')
            ->set_output(
                json_encode(
                    array(
                    	'success' =>$sukes ,
                    	'qty'	=> $jml
                    )
                )
            );
	}

	public function getcart(){
		$this->output
            ->set_content_type('application/json')
            ->set_output(
                json_encode(
                    array('rec' => $this->cart->contents() )
                )
            );		
	}
	
}

/* End of file cart.php */
/* Location: ./application/controllers/cart.php */