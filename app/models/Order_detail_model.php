<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_detail_model extends T_Model {
		
	private $field = array();

    function __construct() {
        parent::__construct();
        $this->set_table('order_detail');
        $this->set_key_field( 'orderd_id' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();
        
        for ($i=0; $i < count($this->field) ; $i++) { 
            $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $fields['user_entry'] = $this->session->userdata('user_name');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }
    
    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->delete_data();
    }

    public function get_rule($insert = true) {
        $rl =  array(
            array(
                'field' => 'order_id',
                'label' => 'Order',
                'width' => 200,
                'grid'  => false,
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'product_id',
                'label' => 'Barang',
                'fieldLabel' => 'product_name',
                'width' => 200,
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'orderd_qty',
                'label' => 'Qty',
                'width' => 70,
                'rules' => 'xss_clean|numeric'
            ),array(
                'field' => 'orderd_price',
                'label' => 'Harga',
                'width' => 100,
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'orderd_total',
                'label' => 'Total',
                'width' => 200,
                'rules' => 'xss_clean|numeric|required'
            )
        );
        if (!$insert) {
            return array_merge(
                array(
                    array(
                        'field' => 'orderd_id',
                        'label' => 'ID',
                        'rules' => 'required|numeric|required'
                    )
                ), $rl
            );
        } else {
            return $rl;
        }
    }
    
    public function search($id){
        $this->set_null();
        $this->set_fields( $this->get_table() . '.*, product_name');
        $this->set_join(array(
            array(
                'TABLE' => 'product',
                'FIELD' => 'product.product_id=' . $this->get_table() . '.product_id',
                'JOIN'  => 'inner'
            )
        ));
        $this->set_params(array('order_id'=>$id));
        return $this->get_data();
    }
}

/* End of file order_detail_model.php */
/* Location: ./application/models/order_detail_model.php */