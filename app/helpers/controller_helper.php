<?php
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: T_controller_helper.php
 */
 if ( ! function_exists('set_headerset_header_grid'))
{
	function set_header_grid($header)
	{
		$arrfields = array();
		foreach ($header as $key => $val) {
		$arrfields[] =
			array(
				'field'=> (isset($val['fieldLabel']))?$val['fieldLabel'] :$val['field'] ,
				'displayName' => $val['label'],
				'resizable' => true,
				'cellTemplate' => isset($val['template'])?$val['template']:'',
				'visible'=> isset($val['grid'])?$val['grid']: true,
				'width'=> isset($val['width'])?$val['width']: '150px', 
				'cellClass'=> isset($val['cellClass'])?$val['cellClass']:'',
			);
	}
	$arrfields[] = array(
				'field'=> 'user_entry' ,
				'displayName' => 'User Entry',
				'resizable' => true,
				'width'=> '150px'
			);
	$arrfields[] = array(
				'field'=> 'date_entry' ,
				'displayName' => 'Tgl Entry',
				'resizable' => true,
				'width'=> '150px'
			);
	$arrfields[] = array(
				'field'=> 'date_edit' ,
				'displayName' => 'Tgl Edit',
				'resizable' => true,
				'width'=> '150px'
			);
	return $arrfields;
	}

	function set_header_aggrid($header)
	{
		$arrfields = array();
		$arrfields[] = array(
			'headerName' => '',
			'width' => 30,
			'checkboxSelection' => true, 
			'suppressSorting' => true, 
			'suppressMenu'=> true
		);
		foreach ($header as $key => $val) {
			$arrfields[] =
				array(
					'field'=> (isset($val['fieldLabel']))?$val['fieldLabel'] :$val['field'] ,
					'headerName' => $val['label'],
					'cellRenderer' => isset($val['cellRenderer'])?$val['cellRenderer']: '',
					'templateUrl' => isset($val['templateUrl'])?$val['templateUrl']: '',
					'template'=> isset($val['template'])?$val['template']: '',
					//'resizable' => true,
					//'cellTemplate' => isset($val['template'])?$val['template']:'',
					'hide'=> isset($val['grid'])?!$val['grid']: !true,
					'width'=> isset($val['width'])?$val['width']: '150', 
					//'cellClass'=> isset($val['cellClass'])?$val['cellClass']:'',
				);
		}
		$arrfields[] = array(
					'field'=> 'user_entry' ,
					'headerName' => 'User Entry',

				);
		$arrfields[] = array(
					'field'=> 'date_entry' ,
					'headerName' => 'Tgl Entry',
					//'template' => "{{data.date_entry | date:'dd-MM-yyyy'}}" ,
				);
		$arrfields[] = array(
					'field'=> 'date_edit' ,
					'headerName' => 'Tgl Edit'
				);
		return $arrfields;
	}
}