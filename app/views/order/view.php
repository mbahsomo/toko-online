<style type="text/css">
    .row-100 , .kiri, .kanan {
        min-height: 100%;
        height: 100%;
        padding-left: 0px;
    }

    .kiri{
        padding-right: 1px;
        padding-left: 15px;
    }

    .atas{
        min-height: 98%;
        height: 98%;

        padding: 0 0 0 0 ;
        margin:  0 0 0 0 ;
        border-left: solid 2px #428ED5;
        border-top: solid 2px #428ED5;
    }

    .gridStyle{
        margin-left: 14px;
    }

    .bawah{
        border-bottom: solid 2px #428ED5;
    }

    .gridStyle1{
        height: 80%;
    }

    .atas .row{
        font-weight: bold;
        color: blue;
    }

</style>
<div class="row">
    <div class="col-md-7 kiri">
        <div style="height: 500px;" ag-grid="gridOptions" class="ag-fresh"></div>        
        <div class="paging-fo" style="bottom:-40px;;">
            <paging
                class="small"
                page="currentPage" 
                page-size="pageSize" 
                total="ttlRec" 
                adjacent="2"
                dots=".."
                scroll-top="false" 
                hide-if-empty="true"
                ul-class="pagination"
                active-class="active"
                disabled-class="disabled"
                show-prev-next="true"
                paging-action="DoCtrlPagingAct('Paging Clicked', page, pageSize, total)">
            </paging>
            <div class="total">Total Record : {{ttlRec}}</div>
        </div>
    </div>
    <div class="col-md-5 kanan">
        <div class="atas" >
            <div style="height: 500px;" ag-grid="gridOptionsDetail" class="ag-fresh"></div>  
        </div> 
    </div>
</div>


<!--Form -->
<div class="modal fade bootstrap-dialog type-primary" id="window-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"> Add / Edit data</h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmProgram" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-floppy-disk"></span> {{setlang.Translet('save')}}</button>
                <button type="button" ng-click="CancelSave()" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-folder-open"></span> {{setlang.Translet('close')}}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Filter Form -->
<div class="modal fade bootstrap-dialog type-primary" id="window-filter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="glyphicon glyphicon-filter"></i> Filter</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group form-group-sm" ng-repeat="(key, value) in gridOptions.columnDefs" >
                        <label for="input{{value.field}}" class="col-sm-3 control-label">{{value.headerName}}</label>
                        <div class="col-sm-9">
                            <input de-focus type="text" class="form-control input-sm" id="input{{value.field}}" placeholder="{{value.headerName}}" ng-model="fields[value.field]">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" ng-click="FilterData()" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-filter"></i> {{setlang.Translet('filter')}}</button>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> {{setlang.Translet('close')}}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<toaster-container toaster-options="{'time-out': 1000, 'close-button':true,'position-class':'toast-top-center'}"></toaster-container>
<script type="text/javascript">
    $(".modal").draggable({
        handle: ".modal-header"
    });
</script>