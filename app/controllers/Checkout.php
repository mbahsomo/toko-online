<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: Checkout.php
 */
class Checkout extends T_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($this->session->userdata('login')){
			$data['isi'] = $this->load->view('frontend/checkout',null,true);
			$this->load->view('frontend/template/index',$data);
		}else{
			redirect('login', 'refresh');
		}
	}

	public function getbiayakirim(){
		//Ambil data alamat pembeli
		$this->load->model('User_model');
		$this->User_model->set_null();
		$this->User_model->set_fields('*');
		$this->User_model->set_params(array('user_email'=>$this->session->userdata('name')));
		$user = $this->User_model->get_data();
		//Hitung biaya kirim barang
		$this->load->model('Shipping_price_model');
		$this->Shipping_price_model->set_null();
		$this->Shipping_price_model->set_fields('price_harga');
		$this->Shipping_price_model->set_params(array(
			'price_state_name' => $user[0]['user_state'],
			'price_city_name' => $user[0]['user_city']
		));
		$biaya = $this->Shipping_price_model->get_data();
		$this->output
            ->set_content_type('application/json')
            ->set_output(
                json_encode(
                    array(
                    	'biaya' => $biaya[0]['price_harga'] ,
                    	'user' => $user[0]
                	)
                )
            );
	}


	public function simpan(){
		$this->load->model('User_model');
		$this->User_model->set_null();
		$this->User_model->set_fields('user_state, user_city');
		$this->User_model->set_params(array('user_email'=>$this->session->userdata('name')));
		$user = $this->User_model->get_data();
		//Hitung biaya kirim barang
		$this->load->model('Shipping_price_model');
		$this->Shipping_price_model->set_null();
		$this->Shipping_price_model->set_fields('price_harga');
		$this->Shipping_price_model->set_params(array(
			'price_state_name' => $user[0]['user_state'],
			'price_city_name' => $user[0]['user_city']
		));
		$biaya = $this->Shipping_price_model->get_data();

		//Simpan dalam transaksi data
		$this->load->model('Order_model');
		$this->Order_model->set_null();
		$this->Order_model->set_fields(array(
			'user_email' => $this->session->userdata('name'), 
			'order_date' => date('Y-m-d H:i:s'),
			'order_total' => $this->cart->total(),
			'order_shipprice' => $biaya[0]['price_harga'],
			'order_status' => 'O'
		));
		$this->Order_model->save_data();
		$session['noorder'] = $this->Order_model->get_rec_id();
        $this->session->set_userdata($session); 

        //Simpan Order Detail
        $this->load->model('Order_detail_model');
        foreach ($this->cart->contents() as $items)
        {        	
        	$this->Order_detail_model->set_null();
        	$this->Order_detail_model->set_fields(array(
        		'order_id' => $this->session->userdata('noorder'),
        		'product_id' => $items['id'],
        		'orderd_qty' => $items['qty'],
        		'orderd_price' => $items['price'],
        		'orderd_total' => $items['qty'] * $items['price']
    		));
    		$this->Order_detail_model->save_data();
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(
                json_encode(
                    array(
                    	'success' => true,
                    	'url' => site_url('checkout/kwitansi/')
                	)
                )
            );        
		
	}

	
	public function kwitansi()
	{
		//echo $this->session->userdata('noorder');
		$this->cart->destroy();
		$this->load->model('Order_model');

		$this->Order_model->set_fields("*");
		/*$this->Order_model->set_fields('*');*/
		$this->Order_model->set_join(array(
			
			array(
                'TABLE' => 'order_detail',
                'FIELD' => 'order_detail.order_id=order.order_id',
                'JOIN'  =>  'inner'
            ),array(
                'TABLE' => 'user',
                'FIELD' => 'user.user_email=order.user_email',
                'JOIN'  =>  'inner'
            )
		));
		$this->Order_model->set_params(array('order.order_id'=>$this->session->userdata('noorder')));
		/*$this->Order_model->set_params(array('order.order_id'=>$this->session->userdata('noorder')));*/
		$data['rec'] = $this->Order_model->get_data();
		
		$data['isi'] = $this->load->view('frontend/kwitansi',$data,true);
		$this->load->view('frontend/template/index',$data);
		//echo $this->session->userdata('noorder');
		//exit;
	}
}

/* End of file Checkout.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/thera/app/controllers/Checkout.php */