<?php
//print_r ($this->session->sess_expiration);
?>
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <script type="text/javascript">
            var browser = '<?php echo BROWSER; ?>';
            var base_url = '<?php echo base_url(); ?>';
            var site_url = '<?php echo site_url(); ?>/';
            var setlang = 'ina.json';
        </script>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">

        <title>System Informasi</title>
        <!-- 
        <!--[if lt IE 9]><script src="<?php echo base_url(); ?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyAjUwuuiY750i5DSdw2ISSws1uoUbTDcVQ&sensor=false&libraries=places"-->
    </head>
    <body>
        
        <div id="header" ng-controller="MenuCtrl">
            <div class="title" >
                <b>System Informasi Toko Online</b>
            </div>
            <div class="btn-group" role="group" aria-label="..."  style="float:right; margin-right:5px;">
                <button class="btn btn-success dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                    Option [<?php echo $this->session->userdata('name'); ?>]
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#profile">Update Password</a></li>
                    <li role="presentation" class="divider"></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('main/logout'); ?>">Log Out</a></li>
                </ul>
                
            </div>
        </div>
        <div id="splitterContainer" ng-controller="MenuCtrl">
            <div id="leftPane" class="table-responsive">
                <button  ng-if="show_reload" class="btn btn-primary btn-xs btn-block" ng-click="loadMenu()">Reload Menu</button>
                <span ng-if="doing_async">...loading...</span>
                <shortcut>
                    <abn-tree tree-data="my_data" tree-control="my_tree" on-select="my_tree_handler(branch)" expand-level="3" initial-selection="Granny Smith">
                </abn-tree>    
                </shortcut>
            </div>             
            <div id="rightPane" class="main">
                <ng-view></ng-view>
            </div>
        </div>

        <script src="<?php echo base_url(); ?>assets/lib/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib/angularjs/angular.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/script.js"></script>
        <script src="<?php echo base_url(); ?>assets/app/app.js"></script>
        <script type="text/javascript">
            $(function () {
                /*$('.tree li:has(ul)').addClass('parent_li').find(' > a').attr('title', 'Collapse this branch');
                $('.tree li.parent_li > a').on('click', function (e) {
                    var children = $(this).parent('li.parent_li').find(' > ul > li');

                    if (children.is(":visible")) {
                        console.log('Hide');
                        children.hide('fast');
                        $(this).attr('title', 'Expand this branch').find(' > i').addClass('fa-folder-o').removeClass('fa-folder-open-o');
                    } else {
                        console.log('Show');
                        children.show('fast');
                        $(this).attr('title', 'Collapse this branch').find(' > i').addClass('fa-folder-open-o').removeClass('fa-folder-o');
                    }
                    e.stopPropagation();
                });*/
                /*$('.tree ul ul li').hide();*/
                /*$('#sproject').change(function(){
                    //alert($('#sproject').val());
                    $.ajax({
                        type: "POST",
                        url: '<?php echo site_url('main/save_project'); ?>/' + Math.random() ,
                        data: {kd:$('#sproject').val()},
                        success: function(data){
                            console.log(data);
                        },
                        dataType: 'json'
                    });
                });*/
            });
        </script>
    </body>
</html>