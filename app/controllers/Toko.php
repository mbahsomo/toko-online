<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: Toko.php
 */
class Toko extends T_Controller {

	public function index()
	{
		$this->load->model('Product_categori_model');
		$this->load->model('Product_model');
		$data1['cat'] = $this->Product_categori_model->get_all();
		$data1['barang'] = $this->Product_model->search('product_name','',0,100);
		$data2['isi'] = $this->load->view('frontend/index',$data1,true);
		$this->load->view('frontend/template/index',$data2);
	}

	public function kategory($id,$name)
	{
		$this->load->model('Product_categori_model');
		$this->load->model('Product_model');
		$data1['cat'] = $this->Product_categori_model->get_all();
		$data1['barang'] = $this->Product_model->search('category_id',$id,0,100, false);
		$data2['isi'] = $this->load->view('frontend/index',$data1,true);
		$this->load->view('frontend/template/index',$data2);
	}

	public function logout()
	{
		$this->session->sess_destroy();
        redirect(base_url());
	}
}

/* End of file Toko.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/thera/app/controllers/Toko.php */