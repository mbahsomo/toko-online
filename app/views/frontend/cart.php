<script src="<?php echo base_url('assets/lib/angularjs/angular.min.js'); ?>"></script>
<script type="text/javascript">
	function CartCtrl($scope, $http){
		$scope.rec = [] ;
		$scope.LoadCart = function(){
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('cart/getcart') ; ?>',
				method: "POST"				
			}).success(function(data) {
				if (data !== undefined) {
					$scope.rec = data.rec;
				}
			});
		}
		$scope.LoadCart();

		$scope.EditQty = function(items){
			var vdata = {
				rowid : items.rowid,
				qty : items.qty
			};
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('cart/updatecart') ; ?>',
				method: "POST",
				data: $.param(vdata)				
			}).success(function(data) {
				if (data !== undefined) {
					console.log(data);
				}
			});
		}

		$scope.Remove = function(items, idx){
			var vdata = {
				rowid : items.rowid,
				qty : 0
			};
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('cart/updatecart') ; ?>',
				method: "POST",
				data: $.param(vdata)				
			}).success(function(data) {
				if (data !== undefined) {
					$scope.LoadCart();
				}
			});
		};
		
		$scope.GetTotal = function(){
			var total = 0 ;
			angular.forEach($scope.rec, function(value, key){
				total += (value.price * value.qty);
			});
			return total
		};
	};	
</script>
<section id="cart_items" ng-app>
	<div class="container" ng-controller="CartCtrl">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
			  <li><a href="#">Home</a></li>
			  <li class="active">Shopping Cart</li>
			</ol>
		</div>
		<div class="table-responsive cart_info">
			<table class="table table-condensed">
				<thead>
					<tr class="cart_menu">
						<td class="image">ID Barang</td>
						<td class="description"></td>
						<td class="price">Price</td>
						<td class="quantity">Quantity</td>
						<td class="total">Total</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="items in rec">
						<td class="cart_product">
							#{{items.id}}
						</td>
						<td class="cart_description">
							{{items.name}}
						</td>
						<td class="cart_price">
							<p>Rp. {{items.price | number}}</p>
						</td>
						<td class="cart_quantity">
							<div class="cart_quantity_button">
								<input class="cart_quantity_input" ng-blur="EditQty(items)" type="text" name="quantity" ng-model="items.qty" autocomplete="off" size="4">
							</div>
						</td>
						<td class="cart_total">
							<p class="cart_total_price">Rp. {{items.price*items.qty | number}}</p>
						</td>
						<td class="cart_delete">
							<a ng-click="Remove(items, $index)" class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
						</td>
					</tr>
					<tr>
						<td ></td>
						<td ></td>
						<td ></td>
						<td >Total</td>
						<td >
							<p class="cart_total_price">Rp. {{ GetTotal() | number}}</p>
						</td>
						<td ></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</section>