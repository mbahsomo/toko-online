<?php if ($evt_add == 'T'){ ?>
<button type="button" class="btn btn-primary btn-sm" ng-click="ShowAdd()"><i class="glyphicon glyphicon-plus"></i> {{setlang.Translet('add')}}</button>
<?php } if ($evt_edit == 'T') {?>
<button type="button" class="btn btn-success btn-sm" ng-click="ShowEdit()"><i class="glyphicon glyphicon-pencil"></i> {{setlang.Translet('edit')}}</button>
<?php } if ($evt_delete == 'T') {?>
<button type="button" class="btn btn-danger btn-sm" ng-click="ShowDelete()"><i class="glyphicon glyphicon-remove"></i> {{setlang.Translet('delete')}}</button>
<?php } if ($evt_print == 'T') {?>
<button type="button" class="btn btn-default btn-sm" ng-click="Print()"><i class="glyphicon glyphicon-print"></i> {{setlang.Translet('print')}}</button>
<?php } ?>
<div class="btn-group">
    <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="false">
        Tools <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a class="btn" ng-click="Reload()"><i class="glyphicon glyphicon-refresh"></i> {{setlang.Translet('reload')}}</a>
        </li>
        <li>
            <a class="btn" ng-click="ShowFilter()"><i class="glyphicon glyphicon-filter"></i> {{setlang.Translet('filter')}}</a>
        </li>
        <li>
            <a class="btn" ng-click="ResetFilter()"><i class="glyphicon glyphicon-filter"></i> {{setlang.Translet('reset-filter')}}</a>
        </li>
        <?php if ($evt_print == 'T') {?>
        <li>
            <a class="btn" ng-click="ExportXls()"><i class="fa fa-file-excel-o"></i> {{setlang.Translet('export-xls')}}</a>
        </li>
        <?php }?>
    </ul>
</div>
<style type="text/css">
    .dropdown-menu .btn{
        float:left;
    }
</style>
