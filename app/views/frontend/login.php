<script src="<?php echo base_url('assets/lib/angularjs/angular.min.js'); ?>"></script>
<script type="text/javascript">
	function LoginCtrl($scope, $http){
		$scope.user = 'coba';
		$scope.propinsis = [];
		$scope.kotas = [];

		//User
		$scope.email = "";
		$scope.username ="";
		$scope.password = "";
		$scope.propinsi = "" ;
		$scope.kota = "" ;
		$scope.alamat = "" ;

		$scope.ingat = 'F';

		$http({
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			url: '<?php echo site_url('shipping_price/get_propinsi') ; ?>',
			method: "POST"
		}).success(function(data) {
			if (data !== undefined) {
				$scope.propinsis = data.rec;
				console.log($scope.propinsi);
			}
		});

		$http({
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			url: '<?php echo site_url('shipping_price/get_kota') ; ?>',
			method: "POST"
		}).success(function(data) {
			if (data !== undefined) {
				$scope.kotas = data.rec;
			}
		});

		$scope.Daftar = function(){
			var vparams = {
				user_email : $scope.email,
				user_password : $scope.password,
				user_name : $scope.username,
				user_state : $scope.propinsi,
				user_city : $scope.kota,
				user_address : $scope.alamat,
			};
			
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('register/newuser') ; ?>',
				method: "POST",
				data: $.param(vparams)
			}).success(function(data) {
				if (data !== undefined) {
					alert ('Regsiter Sukes ...');
				}
			});
		}
		
		$scope.Login = function(){
			var vparams = {
				user_email : $scope.email,
				user_password : $scope.password,
				ingat : $scope.ingat
			};
			
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('register/login') ; ?>',
				method: "POST",
				data: $.param(vparams)
			}).success(function(data) {
				if (data !== undefined) {
					if(data.success==true){
						alert ('Login Sukes ...');
						window.location.href = '<?php echo site_url() ;?>';
					}else{
						alert(data.msg);
					}
				}
			});
		}
	}
</script>
<section id="form" ng-app>
	<div class="container" ng-controller="LoginCtrl">
		<div class="row">
			<div class="col-sm-4 col-sm-offset-1">
				<div class="login-form"><!--login form-->
					<h2>Login to your account</h2>
					<form id="frm-login" ng-submit="Login()">
						<input type="email" placeholder="Email Address"  ng-model="email"/>
						<input type="password" placeholder="Password" ng-model="password" />
						<span>
							<input type="checkbox" value="remember-me" name="ingat" ng-model="ingat"> 
							Keep me signed in
						</span>
						<button form="frm-login" type="submit" class="btn btn-default">Login</button>
					</form>
				</div><!--/login form-->
			</div>
			<div class="col-sm-1">
				<h2 class="or">OR</h2>
			</div>
			<div class="col-sm-4">
				<div class="signup-form"><!--sign up form-->
					<h2>Daftar Baru!</h2>
					<form ng-submit="Daftar()" id="frm-daftar">
						<input required type="email" placeholder="Email Address" ng-model="email" />
						<input required type="text" placeholder="Name"/ ng-model="username">
						<input required type="password" placeholder="Password" ng-model="password" />
						<div>
						<select required ng-model = "propinsi">
							<option>Pilih Propinsi</option>
							<option ng-repeat="pr in propinsis" value="{{pr.price_state_name}}">{{pr.price_state_name}}</option>
						</select>
						</div>
						<div style="margin : 10px 0 10px 0;">
						<select required ng-model="kota">
							<option>Pilih Kota</option>
							<option ng-repeat="kt in kotas | filter:propinsi" value="{{kt.price_city_name}}">{{kt.price_city_name}}</option>
						</select><br>
						</div>
						<input required type="text" placeholder="Alamat" ng-model="alamat"/>

						<button type="submit" class="btn btn-default" form="frm-daftar">Signup</button>
					</form>
				</div><!--/sign up form-->
			</div>
		</div>
	</div>
</section><!--/form-->