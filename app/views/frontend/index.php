<script src="<?php echo base_url('assets/lib/angularjs/angular.min.js'); ?>"></script>
<script type="text/javascript">
	function BeliCtrl($scope, $http){
		$scope.IsiCart = function(id){
			$http({
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				url: '<?php echo site_url('cart/add2cart') ; ?>',
				method: "POST",
				data: $.param( {kode:id} )
			}).success(function(data) {
				if (data !== undefined) {
					if (data.success == true) {
						alert ('Sudah di tambahkan ke keranjang belanja');
					}else{
						alert (data.msg);
					}
				}
			});
		}
	};	
</script>

<section ng-app>
	<div class="container" ng-controller="BeliCtrl">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
					<h2>Category</h2>
					<div class="panel-group category-products" id="accordian">
					<?php foreach($cat as $key => $value) {?>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a href="<?php echo site_url('toko/kategory/' . $value['category_id'] .'/' .$value['category_name']); ?>"><?php echo $value['category_name'] ; ?></a></h4>									
							</div>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-9 padding-right">
				<div class="features_items"><!-- Produk -->
					<h2 class="title text-center">PRODUK</h2>
					<?php foreach ($barang as $key => $value) {?>
					<div class="col-sm-4">					
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="<?php echo base_url('assets/images/barang/thumb/' . $value['image']); ?>" alt="Barang" />
									<h2><?php echo number_format($value['product_price']) ; ?></h2>
									<p><?php echo $value['product_name']; ?></p>
								</div>
								<div class="product-overlay">
									<div class="overlay-content">
										<img src="<?php echo base_url('assets/images/barang/thumb/' . $value['image']); ?>" alt="Barang" />
										<h2><?php echo number_format($value['product_price']) ; ?></h2>
										<p><?php echo $value['product_name']; ?></p>
										<a href="#" ng-click="IsiCart(<?php echo $value['product_id']; ?>)" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Masuk Keranjang</a>
										<a href="<?php echo site_url('detail/index/' . $value['product_id'] . '/' . $value['product_name']); ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div><!-- Produk -->
			</div>
		</div>
	</div>
</section>