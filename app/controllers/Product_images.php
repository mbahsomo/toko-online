<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: Product_images.php
 */
class Product_images extends T_Controller {

	var $stop = 0;
    var $menudata = array();

    function __construct() {
        parent::__construct();
        $this->load->model('Product_images_model', 'mdl');
        $this->stop = BATAS_REC;
    }

    public function access_rules() {
        return array(
            array('allow',
                'actions' => array('cetak','load_view','load_controller','index','insert_data','edit_data','delete_data','search','get_newcode','get_menu','edit_menu','export_xls','get_all','get_gambar','uploadfile'),
                'expression' => $this->session->userdata('login')
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }

    public function load_view(){
        $this->load->view( $this->cid . '/view');
    }

    public function load_controller(){
        $this->load->helper('controller_helper');
        $this->load->library('Doevent');
        $doe = new Doevent();
        $doe->hashkey();
        $data['stop'] = $this->stop;
        $data['controller'] =  ucfirst($this->cid) .'Controller';
        $doe->load_view( $this->cid  .'/controller', $data, '<span><div>');
    }

    private function _init(){
        $_POST['product_images_price']  = str_replace(",", "",  $_POST['product_images_price']);
        $_POST['product_images_price'] = str_replace(",", ".", $_POST['product_images_price']);
    }

    public function insert_data() {
    	$this->_init();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            //echo $error;
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => $this->mdl->insert(), "kode" => $this->mdl->get_rec_id())));
        }
    }

    public function edit_data() {
    	/*$_POST['price_harga'] = str_replace(",", "", $_POST['price_harga']);*/
        $this->_init();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => $this->mdl->update($this->input->post($this->mdl->get_key_field(), true)))));
        }
    }

    public function delete_data() {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                                array(
                                    'success' => $this->mdl->delete($this->input->post($this->mdl->get_key_field(), true)),
                                    'max_page' => $this->get_max_page($this->mdl->get_tot_rows(), $this->stop)
                                )
        ));
    }

    public function search() {
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(
            array(
                'success' => true,
                'rec' => $this->mdl->search(
                        $this->input->post('field', true), $this->input->post('value', true), $this->input->post('stop', true), $this->input->post('limit', true)
                ),
                'max_page' => $this->mdl->get_tot_rows()
            )
        ));
    }

    public function get_all() {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    array(
                        'rec' => $this->mdl->get_all('*',array('product_id'=>$this->input->post('id',true)))
                    )
        ));
    }

    public function uploadfile($id)
    {
        $flname = $_FILES['file']['name'];
        $config['upload_path'] = 'assets/images/barang/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['file_name'] = 'gambar-' . $;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file', $flname)) {
            $flname = $this->upload->data();
            log_message('error', 'Error upload = '. $this->upload->display_errors() );
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'errormsg' => $this->upload->display_errors())));
        } else {
            $flname = $this->upload->data();
            $this->_set_thumb($flname['file_name']);
            $this->_save_image($id, $flname['file_name']);
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('nmfile' => $flname['file_name'], 'success' => true, 'msg' => 'Upload suskes')));
        }        
    }

    private  function _save_image($id, $img){
        $this->mdl->set_null();
        $this->mdl->set_fields(array(
            'product_id' => $id,
            'pi_image' => $img , 
            'user_entry' => 'System',
            'date_entry' => date('Y-m-d H:i:s'),
            'date_edit'  => date('Y-m-d H:i:s'),
        ));
        $this->mdl->save_data();
    }

    private function _set_thumb($img)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = 'assets/images/barang/' . $img;
        $config['create_thumb'] = TRUE;
        $config['thumb_marker'] = '';
        $config['new_image'] = 'assets/images/barang/thumb/' . $img;
        $config['maintain_ratio'] = TRUE;
        $config['width']         = 100;
        $config['height']       = 100;

        $this->load->library('image_lib', $config);

        $this->image_lib->resize();
    }
}

/* End of file Product_images.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/thera/app/controllers/Product_images.php */