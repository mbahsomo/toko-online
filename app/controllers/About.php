<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: About.php
 */
class About extends CI_Controller {

	public function index()
	{
		$data['isi'] = $this->load->view('frontend/about',null,true);
		$this->load->view('frontend/template/index',$data);
	}

}

/* End of file About.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/thera/app/controllers/About.php */