-- phpMyAdmin SQL Dump
-- version 4.0.10.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 28, 2015 at 07:34 PM
-- Server version: 5.1.41-3ubuntu12
-- PHP Version: 5.3.2-1ubuntu4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
SET FOREIGN_KEY_CHECKS = 0;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbterra`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_email` varchar(100) NOT NULL,
  `admin_name` varchar(25) NOT NULL,
  `admin_password` varchar(32) NOT NULL,
  PRIMARY KEY (`admin_email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_email`, `admin_name`, `admin_password`) VALUES
('admin@admin.com', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_session` varchar(255) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `cart_qty` int(11) NOT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `fk_cart_product1_idx` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `cart_session`, `product_id`, `cart_qty`) VALUES
(1, NULL, 19, 1),
(2, NULL, 20, 1),
(3, NULL, 20, 1),
(4, NULL, 17, 1),
(5, NULL, 16, 1),
(23, NULL, 19, 1),
(24, NULL, 18, 1),
(25, NULL, 18, 1),
(26, NULL, 18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(100) NOT NULL,
  `order_date` datetime NOT NULL,
  `order_note` text NOT NULL,
  `order_total` double NOT NULL,
  `order_shipprice` double NOT NULL,
  `order_status` char(1) NOT NULL DEFAULT 'O' COMMENT 'O : Order\nC : Cancel\nP : Bayar',
  PRIMARY KEY (`order_id`),
  KEY `fk_order_users1_idx` (`user_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`order_id`, `user_email`, `order_date`, `order_note`, `order_total`, `order_shipprice`, `order_status`) VALUES
(5, '7', '2015-08-03 01:17:50', '', 73000, 8000, 'C'),
(6, '7', '2015-08-05 14:53:01', '', 109000, 8000, 'C'),
(7, '7', '2015-08-05 14:55:27', '', 49000, 8000, 'C'),
(8, '7', '2015-08-06 14:37:03', '', 49500, 8000, 'P'),
(9, 'mbahsomo@gmail.com', '2015-08-16 16:58:55', '', 79500, 8000, 'C'),
(10, 'mbahsomo@gmail.com', '2015-08-21 20:02:46', '', 39500, 14000, 'C'),
(11, 'mbahsomo@gmail.com', '2015-08-26 23:55:09', '', 0, 14000, 'O'),
(12, 'mbahsomo@gmail.com', '2015-08-26 23:59:50', '', 105500, 14000, 'O');

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE IF NOT EXISTS `order_detail` (
  `orderd_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `orderd_qty` int(11) NOT NULL,
  `orderd_price` double NOT NULL,
  `orderd_total` double NOT NULL,
  PRIMARY KEY (`orderd_id`),
  KEY `fk_order_detail_order1_idx` (`order_id`),
  KEY `fk_order_detail_product1_idx` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`orderd_id`, `order_id`, `product_id`, `orderd_qty`, `orderd_price`, `orderd_total`) VALUES
(10, 5, 15, 1, 34500, 34500),
(11, 5, 17, 1, 38500, 38500),
(12, 6, 18, 1, 24000, 24000),
(13, 6, 19, 1, 25000, 25000),
(14, 6, 14, 1, 30000, 30000),
(15, 6, 13, 1, 30000, 30000),
(16, 7, 19, 1, 25000, 25000),
(17, 7, 18, 1, 24000, 24000),
(18, 8, 20, 1, 24500, 24500),
(19, 8, 19, 1, 25000, 25000),
(20, 9, 11, 1, 40000, 40000),
(21, 9, 16, 1, 39500, 39500),
(22, 10, 16, 1, 39500, 39500),
(23, 12, 11, 1, 40000, 40000),
(24, 12, 16, 1, 39500, 39500),
(25, 12, 5, 1, 26000, 26000);

--
-- Triggers `order_detail`
--
DROP TRIGGER IF EXISTS `order_detail_AFTER_DELETE`;
DELIMITER //
CREATE TRIGGER `order_detail_AFTER_DELETE` AFTER DELETE ON `order_detail`
 FOR EACH ROW begin
  UPDATE product set product_qty = product_qty + OLD.orderd_qty
  WHERE product_id = OLD.product_id;
end
//
DELIMITER ;
DROP TRIGGER IF EXISTS `order_detail_AFTER_INSERT`;
DELIMITER //
CREATE TRIGGER `order_detail_AFTER_INSERT` AFTER INSERT ON `order_detail`
 FOR EACH ROW BEGIN
  UPDATE product set product_qty = product_qty - NEW.orderd_qty
  WHERE product_id = NEW.product_id;
  
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `order_detail_AFTER_UPDATE`;
DELIMITER //
CREATE TRIGGER `order_detail_AFTER_UPDATE` AFTER UPDATE ON `order_detail`
 FOR EACH ROW BEGIN
  UPDATE product set product_qty = product_qty + OLD.orderd_qty
  WHERE product_id = OLD.product_id;
  
  UPDATE product set product_qty = product_qty - NEW.orderd_qty
  WHERE product_id = NEW.product_id;
  
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `payment_date` datetime NOT NULL,
  `payment_bankfrom` varchar(25) NOT NULL,
  `payment_bankto` varchar(25) NOT NULL,
  `payment_bankaccount` varchar(25) NOT NULL,
  `payment_transfer` int(11) NOT NULL,
  `paymenyt_transferdate` datetime NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `fk_payment_order1_idx` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Triggers `payment`
--
DROP TRIGGER IF EXISTS `payment_BEFORE_DELETE`;
DELIMITER //
CREATE TRIGGER `payment_BEFORE_DELETE` BEFORE DELETE ON `payment`
 FOR EACH ROW BEGIN
	UPDATE `order` SET order_status ='O' where order_id = OLD.order_id;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `payment_BEFORE_INSERT`;
DELIMITER //
CREATE TRIGGER `payment_BEFORE_INSERT` BEFORE INSERT ON `payment`
 FOR EACH ROW BEGIN
	UPDATE `order` SET order_status ='P' where order_id = NEW.order_id;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(25) NOT NULL,
  `product_desc` text NOT NULL,
  `product_price` double NOT NULL,
  `product_qty` float DEFAULT NULL,
  `user_entry` varchar(100) DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL,
  `date_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `fk_product_product_category_idx` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `category_id`, `product_name`, `product_desc`, `product_price`, `product_qty`, `user_entry`, `date_entry`, `date_edit`) VALUES
(1, 3, 'Suncare With Foundation', 'Digunakan untuk membantu melindungi wajah dari sengatan sinar matahari dan menyamarkan flek hitam pada kulit wajah agar tampak lebih halus dengan SPF 30.\nPerhatian : \nTidak cocok untuk kulit berjerawat \nPOM NA 18111700160', 24000, 10, NULL, NULL, NULL),
(2, 3, 'Suncare KL (Kuning Langsa', 'Kegunaan:\nDigunakan untuk membantu melindungi kulit wajah dari sengatan sinar matahari.', 23000, 10, NULL, NULL, NULL),
(3, 3, 'Suncare KN (Kuning Natura', 'Kegunaan:\nDigunakan untuk membantu melindungi kulit wajah dari sengatan sinar matahari.', 23000, 10, NULL, NULL, NULL),
(4, 3, 'Suncare Green Tea', 'Netto 10 gr\n\nCara pemakaian :\nPada siang hari oleskan secara merata pada kulit wajah, sebelum memakai bedak.', 24500, 10, NULL, NULL, NULL),
(5, 1, 'Cream Komedo', 'Krim racikan yg diformulasikan dengan bahan alami untuk mengatasi komedo terbuka & tertutup. \n\nTiap gram mengandung Salicylic acid 20 mg, Res2 dan Cl2. \nKemanan racikan untuk kehamilan menurut US FDA : kategori C \n(hanya diberikan jika manfaat lebih besar daripada resiko terhadap janin).\n\nDITUJUKAN UNTUK: \nKomedo terbuka -- titik-titik hitam (blackhead)\nKomedo tertutup -- titik-titik putih (whitehead)\n\nTIDAK COCOK UNTUK : Pasien yg sensitif terhadap salah satu komponen obat\n\nCARA KERJA : \nSalicylic acid bekerja sebagai keratolitik, bakterisida, dan komedolitik yg meningkatkan pergantian sel kulit mati, membuka pori yg tersumbat, menetralisir bakteri dalam pori, memperkecil diameter pori, dan memberikan ruang bagi sel kulit baru untuk tumbuh. \n\nCARA PEMAKAIAN :\nOleskan tipis dan merata pada kulit wajah yg dikehendaki 2 kali sehari pada pagi hari sebelum menggunakan tabir surya dan pada malam hari menjelang tidur.', 26000, 9, 'admin@admin.com', NULL, '2015-08-12 18:40:15'),
(6, 3, 'Suncare With AHA', 'POM NA 18111700158\n\nKrim tabir surya yang bermanfaat untuk membantu melindungi kulit wajah dari pengaruh buruk sinar matahari (kulit kemerahan, rasa terbakar, bintik hitam, kekeringan & efek penuaan dini)Diperkaya dengan AHA yang membantu mengangkat sel sel kulit mati sehingga kulit tampak lebih cerah.\n\nCocok untuk kulit normal cenderung keringKrim warna putih, \nSPF 30\nIsi bersih 10 gr\n\nCara pemakaian :\nOleskan krim merata pada kulit wajah sebelum memakai bedak\nUntuk kombinasi pemakaian dengan produk lain harap konsultasikan dengan dokter.\n\nAman untuk ibu hamil & menyusui\n\nHati-hati untuk kulit sensitif. \nSebaiknya hentikan pemakaian bila timbul rasa pedih, panas, gatal & kemerahan kulit', 25000, 10, NULL, NULL, NULL),
(7, 1, 'Theraskin Aloe Vera Cream', 'Kegunaan : \nPelembab ringan yang memberikan rasa dingin di wajah\n\nCara pemakaian : \nPelembab sebaiknya hanya digunakan untuk kulit yang normal atau kering. \nBagi yang mempunyai kulit berminyak tidak disarankan. \nDipakai pada muka / wajah yang sudah dibersihkan, oles merata.', 24500, 10, 'admin@admin.com', NULL, '2015-08-12 18:39:59'),
(8, 1, 'Theraskin Neck Cream', 'Isi : 10gr\n\nDigunakan untuk membantu menjaga kelembaban dan merawat kekencangan kulit leher.\n\nCARA PEMAKAIAN :\nOleskan merata pada kulit leher.\n\nPERHATIAN :\nHentikan pemakaian bila timbul rasa perih, panas, gatal, dan kemerahan di kulit. \nHindari kontak dengan mata.\n\nKANDUNGAN :\nAqua, \nGluconolactone, \nStearic Acid, \nSunflower (Helenathus Annus) Seed Oil. \nTocophenol, \nCetyl Alcohol, \nAcetyl Hexapeptide-8, \nPhenoxyethanol, \nMethylparaben, \nEthylparaben, \nButyl Paraben, \nPropylparaben\n\nPOM NA : 18110101648', 39000, 10, 'admin@admin.com', NULL, '2015-08-12 18:42:00'),
(9, 3, 'Suncare For Normal', 'POM NA 18111700215\n\nKategori produk : SUN PROTECTOR\n\nTheraskin suncare for normal skin mild adalah salah satu produk sun protector yg bermanfaat untuk membantu melindungi kulit dari efek buruk sinar matahari.Mengandung pelindung fisik titanium dioxide dan pelindung organik benzophenone-3, secara total melindungi kulit dari sinar UVA maupun UVB.\n\nCara pemakaian :\nOleskan krim kira-kira seukuran dua biji jagung ke seluruh wajah, termasuk di sekitar mata. \nBisa dilanjutkan dengan pemakaian bedak untuk hasil finishing yg halus tanpa kesan mengkilap dari minyak.\n\nAman untuk kehamilan? Relatif aman. \nBelum ada bukti penelitian tentang toksisitasnya terhadap kehamilan.\n\nPerhatian! Jika Anda beraktivitas di luar ruangan atau banyak berkeringat, sangat disarankan untuk memakai tabir surya spray tiap 2 jam untuk memastikan perlindungan ekstra yang cukup dari efek buruk sinar matahari.', 21500, 10, NULL, NULL, NULL),
(10, 3, 'Suncare For Oily', 'Manufactured by : PT L'' ESSENTIAL, \nPOM NA 18111700217\n\nKrim tabir surya yang bermanfaat untuk membantu melindungi kulit wajah dari pengaruh buruk sinar matahari \n(kulit kemerahan, rasa terbakar, bintik hitam, kekeringan & efek penuaan dini)\n\nCocok untuk kulit berminyak. \nKrim warna putih, \nSPF 15\nIsi bersih 10 gr\n\nCara pemakaian :\nOleskan krim merata pada kulit wajah sebelum memakai bedak. \n\nUntuk kombinasi pemakaian dengan produk lain harap konsultasikan dengan dokter. \n\nAman untuk ibu hamil & menyusui. \nHati-hati untuk kulit sensitif. \nKecil kemungkinan iritasi kulit dengan penggunaan produk ini, tapi hentikan pemakaian bila timbul rasa pedih, panas, gatal & kemerahan kulit.', 21500, 10, NULL, NULL, NULL),
(11, 2, 'Therasin Compact Powder K', 'PT L'' ESSENTIAL, POM NA 18131205189\n\nKombinasi bedak, foundation dan tabir surya.\nBermanfaat untuk memberikan warna kulit yang rata, tampilan yang menawan dan melindungi kulit dari pengaruh buruk sinar matahari (kulit kemerahan, rasa terbakar, bintik hitam, kekeringan & efek penuaan dini)\n\nCocok untuk perawatan kulit dengan masalah flek, pigmentasi atau warna yang tidak rata. \nBedak warna kuning natural, SPF 15\n\nIsi bersih 14 gr\n\nCara pemakaian :\nOleskan menggunakan spons secara merata ke seluruh kulit wajah. \n\nIdealnya digunakan setelah pemakaian tabir surya. Untuk kombinasi pemakaian dengan produk lain harap konsultasikan dengan dokter.\n\nAman untuk ibu hamil & menyusui.\n\nHati-hati untuk kulit sensitif. Kecil kemungkinan iritasi kulit dengan penggunaan produk ini, tapi hentikan pemakaian bila timbul rasa pedih, panas, gatal & kemerahan kulit. \n\nHarap tambahan plastik buble dlm packingan.', 40000, 9, 'admin@admin.com', NULL, '2015-08-16 17:00:39'),
(12, 2, 'Therasin Compact Powder K', 'PT L'' ESSENTIAL, POM NA 18131205189\n\nKombinasi bedak, foundation dan tabir surya.\nBermanfaat untuk memberikan warna kulit yang rata, tampilan yang menawan dan melindungi kulit dari pengaruh buruk sinar matahari (kulit kemerahan, rasa terbakar, bintik hitam, kekeringan & efek penuaan dini)\n\nCocok untuk perawatan kulit dengan masalah flek, pigmentasi atau warna yang tidak rata. \nBedak warna kuning langsat, SPF 15\n\nIsi bersih 14 gr\n\nCara pemakaian :\nOleskan menggunakan spons secara merata ke seluruh kulit wajah. \n\nIdealnya digunakan setelah pemakaian tabir surya. Untuk kombinasi pemakaian dengan produk lain harap konsultasikan dengan dokter.\n\nAman untuk ibu hamil & menyusui.\n\nHati-hati untuk kulit sensitif. Kecil kemungkinan iritasi kulit dengan penggunaan produk ini, tapi hentikan pemakaian bila timbul rasa pedih, panas, gatal & kemerahan kulit. \n\nHarap tambahan plastik buble dlm packingan.', 40000, 10, NULL, NULL, NULL),
(13, 2, 'Theraskin Loose Powder KN', 'PT L'' ESSENTIAL, POM NA 18131205189\n\nTrichlosan 1% dan tabir surya bermanfaat untuk membantu mengurangi bakteri penyebab jerawat dan melindungi kulit dari pengaruh buruk sinar matahari (kulit kemerahan, rasa terbakar, bintik hitam, kekeringan & efek penuaan dini)\n\nCocok untuk perawatan kulit dengan masalah flek, pigmentasi atau warna yang tidak rata. \nBedak warna kuning natural, SPF 15\n\nIsi bersih 14 gr\n\nCara pemakaian :\nOleskan menggunakan spons secara merata ke seluruh kulit wajah. \n\nIdealnya digunakan setelah pemakaian tabir surya. Untuk kombinasi pemakaian dengan produk lain harap konsultasikan dengan dokter.\n\nAman untuk ibu hamil & menyusui.\n\nHati-hati untuk kulit sensitif. Kecil kemungkinan iritasi kulit dengan penggunaan produk ini, tapi hentikan pemakaian bila timbul rasa pedih, panas, gatal & kemerahan kulit. \n\nHarap tambahan plastik buble dlm packingan.', 30000, 10, NULL, NULL, NULL),
(14, 2, 'Theraskin Loose Powder KL', 'PT L'' ESSENTIAL, POM NA 18131205189\n\nTrichlosan 1% dan tabir surya bermanfaat untuk membantu mengurangi bakteri penyebab jerawat dan melindungi kulit dari pengaruh buruk sinar matahari (kulit kemerahan, rasa terbakar, bintik hitam, kekeringan & efek penuaan dini)\n\nCocok untuk perawatan kulit dengan masalah flek, pigmentasi atau warna yang tidak rata. \nBedak warna kuning langsat, SPF 15\n\nIsi bersih 14 gr\n\nCara pemakaian :\nOleskan menggunakan spons secara merata ke seluruh kulit wajah. \n\nIdealnya digunakan setelah pemakaian tabir surya. Untuk kombinasi pemakaian dengan produk lain harap konsultasikan dengan dokter.\n\nAman untuk ibu hamil & menyusui.\n\nHati-hati untuk kulit sensitif. Kecil kemungkinan iritasi kulit dengan penggunaan produk ini, tapi hentikan pemakaian bila timbul rasa pedih, panas, gatal & kemerahan kulit. \n\nHarap tambahan plastik buble dlm packingan.', 30000, 10, NULL, NULL, NULL),
(15, 2, 'Theraskin Lip Balm', 'Theraskin Lip Balm / Stick Pelembab Bibir digunakan untuk memberikan nutrisi dan menjaga kelembaban bibir.\n\nPemakaian : \nDipakai sehari-hari sebelum memakai lipstik.\n\nPerhatian :\nHentikan pemakaian bila timbul rasa pedih, panas, gatal atau kemerahan dikulit.\n\n\nDiproduksi dan didistribusikan oleh :\nPT. L''ESSENTIAL \nPOM NA 18121302330', 34500, 11, NULL, NULL, NULL),
(16, 1, 'Theraskin AHA Foot Gel', 'Theraskin Aha Foot Gel digunakan untuk membantu melembabkan dan menghaluskan tumit kaki yang pecah pecah. \n\nPemakaian :\nSelama penggunaan hindari langsung kontak dengan sinar matahari, \nJangan digunakan disekitar mata, mulut dan membran mukosa lainnya.\n\nPerhatian :\nHentikan pemakaian bila timbul rasa pedih, panas, gatal dan kemerahan dikulit.\n\nPOM NA 18110101853', 39500, 9, 'admin@admin.com', NULL, '2015-08-12 18:43:14'),
(17, 1, 'Theraskin Dark Circle Cre', 'POM NA 18110101295\n\nIsi : 10gr\n\nDigunakan untuk membantu menyamarkan lingkar hitam mata & garis halus pada kulit sekitar mata.\n\nPEMAKAIAN :\n\nBesihkan wajah dan keringkan, kemudian oleskan Dark Circle Cream secara tipis dan merata pada kulit sekitar mata.\n\nPERHATIAN : \nHentikan pemakaian bila timbul rasa perih, panas, gatal, dan kemerahan di kulit. \nHindari kontak dengan mata.\n\nKANDUNGAN :\nAqua, \nStearic Acid, \nSunflower (Helianthus Annuuss) Seed Oil, \nTocophenol, \nGluconolactone, \nCetyl Alcohol, \nTriethanolamine, \nCetearyl Alcohol, \nPolyacrylamide, \nC13-14 Isoparaffin, \nLaureth-7, \nDimethicone, \nGlyceryl Stearate, \nCeteareth-20, \nCeteareth-12, \nCetyl Palmitate, \nButylene Glycol, \nCantella Asiantica Extract', 38500, 11, NULL, NULL, NULL),
(18, 3, 'Suncare With Foundation', 'Digunakan untuk membantu melindungi wajah dari sengatan sinar matahari dan menyamarkan flek hitam pada kulit wajah agar tampak lebih halus dengan spf 30.\n\nPerhatian :\nTidak cocok untuk kulit berjerawat \n\nPOM NA 18111700160', 24000, 10, NULL, NULL, NULL),
(19, 3, 'Theraskin Suncare With Br', 'POM NA 18111700155 \n\nKrim warna pink. \n\nKrim tabir surya yg bermanfaat untuk membantu melindungi kulit dari pengaruh buruk sinar UV sekaligus membantu membuat kulit tampak lebih cerah.Lembut untuk kulit sensitif. \n\nGunakan pagi hari dan setiap kali akan terpapar sinar matahari pada seluruh wajah dan leher.', 25000, 10, NULL, NULL, NULL),
(20, 1, 'Theraskin Centac Cream', 'POM NA 18110101485\n\nBekas jerawat dapat meninggalkan lubang atau bopeng yang tentunya terlihat tidak cantik. \nCara paling baik adalah untuk menghindari terjadinya lubang atau bopeng bekas jerawat. \nYaitu dengan mengobati jerawat ketika baru mulai terlihat sehingga tidak meninggalkan bekas.\nJika lubang / bopeng sudah terjadi, cara satu-satunya yang paling ampuh adalah dengan memaksa tumbuhnya lapisan kulit wajah yang baru. \nCream ini mampu mengaktifkan fibroblast (sel hidup) untuk membuat jaringan baru di wajah yang berbekas.\nBopeng dan bekas jerawat sangat merusak penampilan dan percaya diri. \nSegera atasi hal ini. Gunakan rutin dan raih hasilnya.\n\nBERFUNGSI :\nMerangsang pertumbuhan sel baru di area bekas jerawat\nMengisi jaringan kolagen yang kosong akibat pemencetan yang mengakibatkan bekas jerawat\n\nCARA PEMAKAIAN :\nOleskan Cream pada bagian bekas jerawat secara teratur pada malam hari.\nBuat yg punya scar / bopeng karena jerawat sebelum memutuskan untuk dermaroller/dermapunch, \nboleh dicoba nih scar cream dg kandungan cantella asiatica bikin wajah jadi lebih halus.', 24500, 10, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) NOT NULL,
  `user_entry` varchar(100) DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL,
  `date_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`category_id`, `category_name`, `user_entry`, `date_entry`, `date_edit`) VALUES
(1, 'Perawatan Kulit', NULL, NULL, NULL),
(2, 'Kosmetik', NULL, NULL, NULL),
(3, 'Suncare', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `pi_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `pi_image` varchar(50) DEFAULT NULL,
  `user_entry` varchar(45) NOT NULL,
  `date_entry` timestamp NULL DEFAULT NULL,
  `date_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`pi_id`),
  KEY `fk_product_images_product1_idx` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`pi_id`, `product_id`, `pi_image`, `user_entry`, `date_entry`, `date_edit`) VALUES
(5, 7, 'Selection_0501.jpg', 'System', '2015-08-12 11:27:12', '2015-08-12 18:27:12'),
(7, 7, 'Selection_061.jpg', 'System', '2015-08-12 11:38:59', '2015-08-12 18:38:59'),
(8, 7, 'Screenshot_from_2015-08-08_07:45:05.png', 'System', '2015-08-12 11:39:32', '2015-08-12 18:39:32'),
(9, 7, 'Selection_052.jpg', 'System', '2015-08-12 11:39:59', '2015-08-12 18:39:59'),
(11, 8, 'Selection_057.jpg', 'System', '2015-08-12 11:42:00', '2015-08-12 18:42:00'),
(12, 16, 'Selection_042.jpg', 'System', '2015-08-12 11:43:05', '2015-08-12 18:43:05'),
(13, 16, 'makkah.jpg', 'System', '2015-08-12 11:43:14', '2015-08-12 18:43:14'),
(15, 11, 'Selection_033.jpg', 'System', '2015-08-12 11:43:32', '2015-08-12 18:43:32'),
(16, 11, 'departemen-keuangan.jpg', 'System', '2015-08-16 10:00:22', '2015-08-16 17:00:22');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_price`
--

CREATE TABLE IF NOT EXISTS `shipping_price` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `price_state_name` varchar(50) DEFAULT NULL,
  `price_city_name` varchar(50) NOT NULL,
  `price_harga` double NOT NULL DEFAULT '0',
  `user_entry` varchar(100) DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL,
  `date_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=467 ;

--
-- Dumping data for table `shipping_price`
--

INSERT INTO `shipping_price` (`price_id`, `price_state_name`, `price_city_name`, `price_harga`, `user_entry`, `date_entry`, `date_edit`) VALUES
(1, 'BALI', 'KAB. BADUNG', 27000, NULL, NULL, NULL),
(2, 'BALI', 'KAB. BANGLI', 27000, NULL, NULL, NULL),
(3, 'BALI', 'KAB. BULELENG', 27000, NULL, NULL, NULL),
(4, 'BALI', 'KAB. GIANYAR', 27000, NULL, NULL, NULL),
(5, 'BALI', 'KAB. JEMBRANA', 27000, NULL, NULL, NULL),
(6, 'BALI', 'KAB. KARANGASEM', 27000, NULL, NULL, NULL),
(7, 'BALI', 'KAB. KLUNGKUNG', 27000, NULL, NULL, NULL),
(8, 'BALI', 'KAB. TABANAN', 27000, NULL, NULL, NULL),
(9, 'BALI', 'KOTA DENPASAR', 21000, NULL, NULL, NULL),
(10, 'BANGKA BELITUNG', 'KAB. BANGKA', 45000, NULL, NULL, NULL),
(11, 'BANGKA BELITUNG', 'KAB. BANGKA BARAT', 45000, NULL, NULL, NULL),
(12, 'BANGKA BELITUNG', 'KAB. BANGKA SELATAN', 45000, NULL, NULL, NULL),
(13, 'BANGKA BELITUNG', 'KAB. BANGKA TENGAH', 45000, NULL, NULL, NULL),
(14, 'BANGKA BELITUNG', 'KAB. BELITUNG', 40000, NULL, NULL, NULL),
(15, 'BANGKA BELITUNG', 'KAB. BELITUNG TIMUR', 42000, NULL, NULL, NULL),
(16, 'BANGKA BELITUNG', 'KOTA PANGKAL PINANG', 33000, NULL, NULL, NULL),
(17, 'BANTEN', 'KAB. LEBAK', 21000, NULL, NULL, NULL),
(18, 'BANTEN', 'KAB. PANDEGLANG', 21000, NULL, NULL, NULL),
(19, 'BANTEN', 'KAB. SERANG', 19000, NULL, NULL, NULL),
(20, 'BANTEN', 'KAB. TANGERANG', 17000, NULL, NULL, NULL),
(21, 'BANTEN', 'KOTA CILEGON', 19000, NULL, NULL, NULL),
(22, 'BANTEN', 'KOTA SERANG', 19000, NULL, NULL, NULL),
(23, 'BANTEN', 'KOTA TANGERANG', 17000, NULL, NULL, NULL),
(24, 'BENGKULU', 'KAB. BENGKULU SELATAN', 52000, NULL, NULL, NULL),
(25, 'BENGKULU', 'KAB. BENGKULU UTARA', 52000, NULL, NULL, NULL),
(26, 'BENGKULU', 'KAB. KAUR', 52000, NULL, NULL, NULL),
(27, 'BENGKULU', 'KAB. KEPAHIANG', 52000, NULL, NULL, NULL),
(28, 'BENGKULU', 'KAB. LEBONG', 52000, NULL, NULL, NULL),
(29, 'BENGKULU', 'KAB. MUKO-MUKO', 52000, NULL, NULL, NULL),
(30, 'BENGKULU', 'KAB. REJANG LEBONG', 52000, NULL, NULL, NULL),
(31, 'BENGKULU', 'KAB. SELUMA', 52000, NULL, NULL, NULL),
(32, 'BENGKULU', 'KOTA BENGKULU', 37000, NULL, NULL, NULL),
(33, 'D.I. YOGYAKARTA', 'KAB. BANTUL', 13000, NULL, NULL, NULL),
(34, 'D.I. YOGYAKARTA', 'KAB. GUNUNG KIDUL', 22000, NULL, NULL, NULL),
(35, 'D.I. YOGYAKARTA', 'KAB. KULON PROGO', 17000, NULL, NULL, NULL),
(36, 'D.I. YOGYAKARTA', 'KAB. SLEMAN', 13000, NULL, NULL, NULL),
(37, 'D.I. YOGYAKARTA', 'KOTA YOGYAKARTA', 13000, NULL, NULL, NULL),
(38, 'DKI JAKARTA', 'DKI JAKARTA', 14000, NULL, NULL, NULL),
(39, 'DKI JAKARTA', 'KOTA ADMINISTRASI JAKARTA BARAT', 14000, NULL, NULL, NULL),
(40, 'DKI JAKARTA', 'KOTA ADMINISTRASI JAKARTA PUSAT', 14000, NULL, NULL, NULL),
(41, 'DKI JAKARTA', 'KOTA ADMINISTRASI JAKARTA SELATAN', 14000, NULL, NULL, NULL),
(42, 'DKI JAKARTA', 'KOTA ADMINISTRASI JAKARTA TIMUR', 14000, NULL, NULL, NULL),
(43, 'DKI JAKARTA', 'KOTA ADMINISTRASI JAKARTA UTARA', 14000, NULL, NULL, NULL),
(44, 'DKI JAKARTA', 'KOTA ADMINISTRASI KEPULAUAN SERIBU', 14000, NULL, NULL, NULL),
(45, 'GORONTALO', 'KAB. BOALEMO', 80000, NULL, NULL, NULL),
(46, 'GORONTALO', 'KAB. GORONTALO', 80000, NULL, NULL, NULL),
(47, 'GORONTALO', 'KAB. GORONTALO UTARA', 80000, NULL, NULL, NULL),
(48, 'GORONTALO', 'KAB. PAHUWALO', 80000, NULL, NULL, NULL),
(49, 'GORONTALO', 'KOTA BONE BOLANGO', 80000, NULL, NULL, NULL),
(50, 'GORONTALO', 'KOTA GORONTALO', 55000, NULL, NULL, NULL),
(51, 'JAMBI', 'KAB. BATANG HARI', 58000, NULL, NULL, NULL),
(52, 'JAMBI', 'KAB. BUNGO', 58000, NULL, NULL, NULL),
(53, 'JAMBI', 'KAB. KERINCI', 58000, NULL, NULL, NULL),
(54, 'JAMBI', 'KAB. MERANGIN', 58000, NULL, NULL, NULL),
(55, 'JAMBI', 'KAB. MUARO JAMBI', 58000, NULL, NULL, NULL),
(56, 'JAMBI', 'KAB. SAROLANGUN', 58000, NULL, NULL, NULL),
(57, 'JAMBI', 'KAB. TANJUNG JABUNG BARAT', 58000, NULL, NULL, NULL),
(58, 'JAMBI', 'KAB. TANJUNG JABUNG TIMUR', 58000, NULL, NULL, NULL),
(59, 'JAMBI', 'KAB. TEBO', 58000, NULL, NULL, NULL),
(60, 'JAMBI', 'KOTA JAMBI', 32000, NULL, NULL, NULL),
(61, 'JAWA BARAT', 'KAB. BANDUNG', 19000, NULL, NULL, NULL),
(62, 'JAWA BARAT', 'KAB. BANDUNG BARAT', 19000, NULL, NULL, NULL),
(63, 'JAWA BARAT', 'KAB. BEKASI', 17000, NULL, NULL, NULL),
(64, 'JAWA BARAT', 'KAB. BOGOR', 17000, NULL, NULL, NULL),
(65, 'JAWA BARAT', 'KAB. CIANJUR', 21000, NULL, NULL, NULL),
(66, 'JAWA BARAT', 'KAB. CIREBON', 21000, NULL, NULL, NULL),
(67, 'JAWA BARAT', 'KAB. GARUT', 21000, NULL, NULL, NULL),
(68, 'JAWA BARAT', 'KAB. INDRAMAYU', 21000, NULL, NULL, NULL),
(69, 'JAWA BARAT', 'KAB. KARAWANG', 18000, NULL, NULL, NULL),
(70, 'JAWA BARAT', 'KAB. KUNINGAN', 21000, NULL, NULL, NULL),
(71, 'JAWA BARAT', 'KAB. MAJALENGKA', 21000, NULL, NULL, NULL),
(72, 'JAWA BARAT', 'KAB. PURWAKARTA', 21000, NULL, NULL, NULL),
(73, 'JAWA BARAT', 'KAB. SUBANG', 27000, NULL, NULL, NULL),
(74, 'JAWA BARAT', 'KAB. SUKABUMI', 21000, NULL, NULL, NULL),
(75, 'JAWA BARAT', 'KAB. SUMEDANG', 21000, NULL, NULL, NULL),
(76, 'JAWA BARAT', 'KAB. TASIKMALAYA', 21000, NULL, NULL, NULL),
(77, 'JAWA BARAT', 'KAB.CIAMIS', 21000, NULL, NULL, NULL),
(78, 'JAWA BARAT', 'KOTA BANDUNG', 19000, NULL, NULL, NULL),
(79, 'JAWA BARAT', 'KOTA BANJAR', 21000, NULL, NULL, NULL),
(80, 'JAWA BARAT', 'KOTA BEKASI', 17000, NULL, NULL, NULL),
(81, 'JAWA BARAT', 'KOTA BOGOR', 17000, NULL, NULL, NULL),
(82, 'JAWA BARAT', 'KOTA CIMAHI', 19000, NULL, NULL, NULL),
(83, 'JAWA BARAT', 'KOTA CIREBON', 19000, NULL, NULL, NULL),
(84, 'JAWA BARAT', 'KOTA DEPOK', 17000, NULL, NULL, NULL),
(85, 'JAWA BARAT', 'KOTA SUKABUMI', 21000, NULL, NULL, NULL),
(86, 'JAWA BARAT', 'KOTA TASIKMALAYA', 21000, NULL, NULL, NULL),
(87, 'JAWA TENGAH', 'KAB. BANJARNEGARA', 17000, NULL, NULL, NULL),
(88, 'JAWA TENGAH', 'KAB. BANYUMAS', 17000, NULL, NULL, NULL),
(89, 'JAWA TENGAH', 'KAB. BATANG', 17000, NULL, NULL, NULL),
(90, 'JAWA TENGAH', 'KAB. BLORA', 17000, NULL, NULL, NULL),
(91, 'JAWA TENGAH', 'KAB. BOJONEGORO', 17000, NULL, NULL, NULL),
(92, 'JAWA TENGAH', 'KAB. BOYOLALI', 17000, NULL, NULL, NULL),
(93, 'JAWA TENGAH', 'KAB. BREBES', 17000, NULL, NULL, NULL),
(94, 'JAWA TENGAH', 'KAB. CILACAP', 13000, NULL, NULL, NULL),
(95, 'JAWA TENGAH', 'KAB. DEMAK', 17000, NULL, NULL, NULL),
(96, 'JAWA TENGAH', 'KAB. GROBOGAN', 22000, NULL, NULL, NULL),
(97, 'JAWA TENGAH', 'KAB. JEPARA', 17000, NULL, NULL, NULL),
(98, 'JAWA TENGAH', 'KAB. KARANG ANYAR', 17000, NULL, NULL, NULL),
(99, 'JAWA TENGAH', 'KAB. KEBUMEN', 17000, NULL, NULL, NULL),
(100, 'JAWA TENGAH', 'KAB. KENDAL', 17000, NULL, NULL, NULL),
(101, 'JAWA TENGAH', 'KAB. KLATEN', 17000, NULL, NULL, NULL),
(102, 'JAWA TENGAH', 'KAB. KUDUS', 17000, NULL, NULL, NULL),
(103, 'JAWA TENGAH', 'KAB. MAGELANG', 17000, NULL, NULL, NULL),
(104, 'JAWA TENGAH', 'KAB. PATI', 17000, NULL, NULL, NULL),
(105, 'JAWA TENGAH', 'KAB. PEKALONGAN', 22000, NULL, NULL, NULL),
(106, 'JAWA TENGAH', 'KAB. PEMALANG', 17000, NULL, NULL, NULL),
(107, 'JAWA TENGAH', 'KAB. PURBALINGGA', 17000, NULL, NULL, NULL),
(108, 'JAWA TENGAH', 'KAB. PURWOREJO', 17000, NULL, NULL, NULL),
(109, 'JAWA TENGAH', 'KAB. REMBANG', 22000, NULL, NULL, NULL),
(110, 'JAWA TENGAH', 'KAB. SEMARANG', 13000, NULL, NULL, NULL),
(111, 'JAWA TENGAH', 'KAB. SRAGEN', 17000, NULL, NULL, NULL),
(112, 'JAWA TENGAH', 'KAB. SUKOHARJO', 22000, NULL, NULL, NULL),
(113, 'JAWA TENGAH', 'KAB. TEGAL', 17000, NULL, NULL, NULL),
(114, 'JAWA TENGAH', 'KAB. TEMANGGUNG', 17000, NULL, NULL, NULL),
(115, 'JAWA TENGAH', 'KAB. WONOGIRI', 17000, NULL, NULL, NULL),
(116, 'JAWA TENGAH', 'KAB. WONOSOBO', 17000, NULL, NULL, NULL),
(117, 'JAWA TENGAH', 'KOTA MAGELANG', 13000, NULL, NULL, NULL),
(118, 'JAWA TENGAH', 'KOTA PEKALONGAN', 17000, NULL, NULL, NULL),
(119, 'JAWA TENGAH', 'KOTA SALATIGA', 17000, NULL, NULL, NULL),
(120, 'JAWA TENGAH', 'KOTA SEMARANG', 13000, NULL, NULL, NULL),
(121, 'JAWA TENGAH', 'KOTA SURAKARTA', 13000, NULL, NULL, NULL),
(122, 'JAWA TENGAH', 'KOTA TEGAL', 17000, NULL, NULL, NULL),
(123, 'JAWA TIMUR', 'KAB SITUBONDO', 13000, NULL, NULL, NULL),
(124, 'JAWA TIMUR', 'KAB. BANGKALAN', 10000, NULL, NULL, NULL),
(125, 'JAWA TIMUR', 'KAB. BANYUWANGI', 13000, NULL, NULL, NULL),
(126, 'JAWA TIMUR', 'KAB. BLITAR', 13000, NULL, NULL, NULL),
(127, 'JAWA TIMUR', 'KAB. BONDOWOSO', 13000, NULL, NULL, NULL),
(128, 'JAWA TIMUR', 'KAB. JEMBER', 8000, NULL, NULL, NULL),
(129, 'JAWA TIMUR', 'KAB. JOMBANG', 10000, NULL, NULL, NULL),
(130, 'JAWA TIMUR', 'KAB. KEDIRI', 8000, NULL, NULL, NULL),
(131, 'JAWA TIMUR', 'KAB. LAMONGAN', 10000, NULL, NULL, NULL),
(132, 'JAWA TIMUR', 'KAB. LUMAJANG', 13000, NULL, NULL, NULL),
(133, 'JAWA TIMUR', 'KAB. MADIUN', 13000, NULL, NULL, NULL),
(134, 'JAWA TIMUR', 'KAB. MAGETAN', 13000, NULL, NULL, NULL),
(135, 'JAWA TIMUR', 'KAB. MALANG', 13000, NULL, NULL, NULL),
(136, 'JAWA TIMUR', 'KAB. MOJOKERTO', 8000, NULL, NULL, NULL),
(137, 'JAWA TIMUR', 'KAB. NGANJUK', 10000, NULL, NULL, NULL),
(138, 'JAWA TIMUR', 'KAB. NGAWI', 13000, NULL, NULL, NULL),
(139, 'JAWA TIMUR', 'KAB. PACITAN', 13000, NULL, NULL, NULL),
(140, 'JAWA TIMUR', 'KAB. PAMEKASAN', 10000, NULL, NULL, NULL),
(141, 'JAWA TIMUR', 'KAB. PASURUAN', 8000, NULL, NULL, NULL),
(142, 'JAWA TIMUR', 'KAB. PONOROGO', 13000, NULL, NULL, NULL),
(143, 'JAWA TIMUR', 'KAB. PROBOLINGGO', 17000, NULL, NULL, NULL),
(144, 'JAWA TIMUR', 'KAB. SAMPANG', 10000, NULL, NULL, NULL),
(145, 'JAWA TIMUR', 'KAB. SIDOARJO', 8000, NULL, NULL, NULL),
(146, 'JAWA TIMUR', 'KAB. SUMENEP', 10000, NULL, NULL, NULL),
(147, 'JAWA TIMUR', 'KAB. TRENGGALEK', 10000, NULL, NULL, NULL),
(148, 'JAWA TIMUR', 'KAB. TUBAN', 10000, NULL, NULL, NULL),
(149, 'JAWA TIMUR', 'KAB. TULUNGAGUNG', 10000, NULL, NULL, NULL),
(150, 'JAWA TIMUR', 'KAB.GRESIK', 5000, NULL, NULL, NULL),
(151, 'JAWA TIMUR', 'KOTA BATU', 13000, NULL, NULL, NULL),
(152, 'JAWA TIMUR', 'KOTA BLITAR', 13000, NULL, NULL, NULL),
(153, 'JAWA TIMUR', 'KOTA KEDIRI', 8000, NULL, NULL, NULL),
(154, 'JAWA TIMUR', 'KOTA MADIUN', 8000, NULL, NULL, NULL),
(155, 'JAWA TIMUR', 'KOTA MALANG', 8000, NULL, NULL, NULL),
(156, 'JAWA TIMUR', 'KOTA MOJOKERTO', 8000, NULL, NULL, NULL),
(157, 'JAWA TIMUR', 'KOTA PASURUAN', 8000, NULL, NULL, NULL),
(158, 'JAWA TIMUR', 'KOTA PROBOLINGGO', 8000, NULL, NULL, NULL),
(159, 'JAWA TIMUR', 'KOTA SURABAYA', 5000, NULL, NULL, NULL),
(160, 'KALIMANTAN BARAT', 'KAB. BENGKAYANG', 41000, NULL, NULL, NULL),
(161, 'KALIMANTAN BARAT', 'KAB. KAPUAS HULU', 41000, NULL, NULL, NULL),
(162, 'KALIMANTAN BARAT', 'KAB. KAYONG UTARA', 33000, NULL, NULL, NULL),
(163, 'KALIMANTAN BARAT', 'KAB. KETAPANG', 52000, NULL, NULL, NULL),
(164, 'KALIMANTAN BARAT', 'KAB. KUBU RAYA', 33000, NULL, NULL, NULL),
(165, 'KALIMANTAN BARAT', 'KAB. LANDAK', 41000, NULL, NULL, NULL),
(166, 'KALIMANTAN BARAT', 'KAB. MELAWI', 41000, NULL, NULL, NULL),
(167, 'KALIMANTAN BARAT', 'KAB. PONTIANAK', 41000, NULL, NULL, NULL),
(168, 'KALIMANTAN BARAT', 'KAB. SAMBAS', 41000, NULL, NULL, NULL),
(169, 'KALIMANTAN BARAT', 'KAB. SANGGAU', 41000, NULL, NULL, NULL),
(170, 'KALIMANTAN BARAT', 'KAB. SEKADAU', 41000, NULL, NULL, NULL),
(171, 'KALIMANTAN BARAT', 'KAB. SINTANG', 41000, NULL, NULL, NULL),
(172, 'KALIMANTAN BARAT', 'KOTA PONTIANAK', 33000, NULL, NULL, NULL),
(173, 'KALIMANTAN BARAT', 'KOTA SINGKAWANG', 41000, NULL, NULL, NULL),
(174, 'KALIMANTAN SELATAN', 'KAB. BALANGAN', 33000, NULL, NULL, NULL),
(175, 'KALIMANTAN SELATAN', 'KAB. BANJAR', 33000, NULL, NULL, NULL),
(176, 'KALIMANTAN SELATAN', 'KAB. BARITO KUALA', 33000, NULL, NULL, NULL),
(177, 'KALIMANTAN SELATAN', 'KAB. BARITO SELATAN', 33000, NULL, NULL, NULL),
(178, 'KALIMANTAN SELATAN', 'KAB. BARITO TIMUR', 33000, NULL, NULL, NULL),
(179, 'KALIMANTAN SELATAN', 'KAB. BARITO UTARA', 33000, NULL, NULL, NULL),
(180, 'KALIMANTAN SELATAN', 'KAB. HULU SUNGAI SELATAN', 33000, NULL, NULL, NULL),
(181, 'KALIMANTAN SELATAN', 'KAB. HULU SUNGAI TENGAH', 33000, NULL, NULL, NULL),
(182, 'KALIMANTAN SELATAN', 'KAB. HULU SUNGAI UTARA', 33000, NULL, NULL, NULL),
(183, 'KALIMANTAN SELATAN', 'KAB. KOTA BARU', 20000, NULL, NULL, NULL),
(184, 'KALIMANTAN SELATAN', 'KAB. MURUNG RAYA', 33000, NULL, NULL, NULL),
(185, 'KALIMANTAN SELATAN', 'KAB. TABALONG', 42000, NULL, NULL, NULL),
(186, 'KALIMANTAN SELATAN', 'KAB. TAMPIN', 33000, NULL, NULL, NULL),
(187, 'KALIMANTAN SELATAN', 'KAB. TANAH BAMBU', 33000, NULL, NULL, NULL),
(188, 'KALIMANTAN SELATAN', 'KAB. TANAH LAUT', 33000, NULL, NULL, NULL),
(189, 'KALIMANTAN SELATAN', 'KOTA BANJARBARU', 20000, NULL, NULL, NULL),
(190, 'KALIMANTAN SELATAN', 'KOTA BANJARMASIN', 20000, NULL, NULL, NULL),
(191, 'KALIMANTAN TENGAH', 'KAB. GUNUNG MAS', 70000, NULL, NULL, NULL),
(192, 'KALIMANTAN TENGAH', 'KAB. KAPUAS', 56000, NULL, NULL, NULL),
(193, 'KALIMANTAN TENGAH', 'KAB. KATINGAN', 56000, NULL, NULL, NULL),
(194, 'KALIMANTAN TENGAH', 'KAB. KOTAWARINGIN BARAT', 56000, NULL, NULL, NULL),
(195, 'KALIMANTAN TENGAH', 'KAB. KOTAWARINGIN TIMUR', 56000, NULL, NULL, NULL),
(196, 'KALIMANTAN TENGAH', 'KAB. LAMANDAU', 56000, NULL, NULL, NULL),
(197, 'KALIMANTAN TENGAH', 'KAB. PULANG PISAU', 56000, NULL, NULL, NULL),
(198, 'KALIMANTAN TENGAH', 'KAB. SERUYAN', 56000, NULL, NULL, NULL),
(199, 'KALIMANTAN TENGAH', 'KAB. SUKAMARA', 56000, NULL, NULL, NULL),
(200, 'KALIMANTAN TENGAH', 'KOTA PALANGKARAYA', 26000, NULL, NULL, NULL),
(201, 'KALIMANTAN TIMUR', 'KAB. BULUNGAN', 53000, NULL, NULL, NULL),
(202, 'KALIMANTAN TIMUR', 'KAB. KUTAI BARAT', 47000, NULL, NULL, NULL),
(203, 'KALIMANTAN TIMUR', 'KAB. KUTAI KARTANEGARA', 47000, NULL, NULL, NULL),
(204, 'KALIMANTAN TIMUR', 'KAB. MALINAU', 53000, NULL, NULL, NULL),
(205, 'KALIMANTAN TIMUR', 'KAB. NUNUKAN', 53000, NULL, NULL, NULL),
(206, 'KALIMANTAN TIMUR', 'KAB. TANA TIDUNG', 42000, NULL, NULL, NULL),
(207, 'KALIMANTAN TIMUR', 'KOTA SAMARINDA', 34000, NULL, NULL, NULL),
(208, 'KALIMANTAN TIMUR', 'KOTA TARAKAN', 42000, NULL, NULL, NULL),
(209, 'KALIMANTAN TIMUR ', 'KAB. BERAU', 51000, NULL, NULL, NULL),
(210, 'KALIMANTAN TIMUR ', 'KAB. KUTAI TIMUR', 41000, NULL, NULL, NULL),
(211, 'KALIMANTAN TIMUR ', 'KAB. PANAJAM PASER UTARA', 51000, NULL, NULL, NULL),
(212, 'KALIMANTAN TIMUR ', 'KAB. PASER', 51000, NULL, NULL, NULL),
(213, 'KALIMANTAN TIMUR ', 'KOTA BALIKPAPAN', 26000, NULL, NULL, NULL),
(214, 'KALIMANTAN TIMUR ', 'KOTA BONTANG', 40000, NULL, NULL, NULL),
(215, 'KEPULAUAN RIAU', 'KAB. BINTAN', 72000, NULL, NULL, NULL),
(216, 'KEPULAUAN RIAU', 'KAB. KARIMUN', 75000, NULL, NULL, NULL),
(217, 'KEPULAUAN RIAU', 'KAB. LINGGA', 75000, NULL, NULL, NULL),
(218, 'KEPULAUAN RIAU', 'KAB. NATUNA', 94000, NULL, NULL, NULL),
(219, 'KEPULAUAN RIAU', 'KOTA BATAM', 26000, NULL, NULL, NULL),
(220, 'KEPULAUAN RIAU', 'KOTA TANJUNG PINANG', 44000, NULL, NULL, NULL),
(221, 'LAMPUNG', 'KAB. LAMPUNG BARAT', 42000, NULL, NULL, NULL),
(222, 'LAMPUNG', 'KAB. LAMPUNG SELATAN', 42000, NULL, NULL, NULL),
(223, 'LAMPUNG', 'KAB. LAMPUNG TENGAH', 42000, NULL, NULL, NULL),
(224, 'LAMPUNG', 'KAB. LAMPUNG TIMUR', 42000, NULL, NULL, NULL),
(225, 'LAMPUNG', 'KAB. LAMPUNG UTARA', 42000, NULL, NULL, NULL),
(226, 'LAMPUNG', 'KAB. PESAWARAN', 30000, NULL, NULL, NULL),
(227, 'LAMPUNG', 'KAB. TANGGAMUS', 42000, NULL, NULL, NULL),
(228, 'LAMPUNG', 'KAB. TULANG BAWANG', 42000, NULL, NULL, NULL),
(229, 'LAMPUNG', 'KAB. WAY KANAN', 42000, NULL, NULL, NULL),
(230, 'LAMPUNG', 'KOTA BANDAR LAMPUNG', 30000, NULL, NULL, NULL),
(231, 'LAMPUNG', 'KOTA METRO', 42000, NULL, NULL, NULL),
(232, 'MALUKU', 'KAB. BURU', 147000, NULL, NULL, NULL),
(233, 'MALUKU', 'KAB. KEPULAUAN ARU', 276000, NULL, NULL, NULL),
(234, 'MALUKU', 'KAB. MALUKU TENGAH', 147000, NULL, NULL, NULL),
(235, 'MALUKU', 'KAB. MALUKU TENGGARA', 147000, NULL, NULL, NULL),
(236, 'MALUKU', 'KAB. MALUKU TENGGARA BARAT', 147000, NULL, NULL, NULL),
(237, 'MALUKU', 'KAB. SERAM BAGIAN BARAT', 276000, NULL, NULL, NULL),
(238, 'MALUKU', 'KAB. SERAM BAGIAN TIMUR', 276000, NULL, NULL, NULL),
(239, 'MALUKU', 'KOTA AMBON', 55000, NULL, NULL, NULL),
(240, 'MALUKU UTARA', 'KAB. HALMAHERA BARAT', 140000, NULL, NULL, NULL),
(241, 'MALUKU UTARA', 'KAB. HALMAHERA SELATAN', 140000, NULL, NULL, NULL),
(242, 'MALUKU UTARA', 'KAB. HALMAHERA TENGAH', 140000, NULL, NULL, NULL),
(243, 'MALUKU UTARA', 'KAB. HALMAHERA TIMUR', 140000, NULL, NULL, NULL),
(244, 'MALUKU UTARA', 'KAB. HALMAHERA UTARA', 140000, NULL, NULL, NULL),
(245, 'MALUKU UTARA', 'KAB. KEPULAUAN SULA', 140000, NULL, NULL, NULL),
(246, 'MALUKU UTARA', 'KOTA TERNATE', 57000, NULL, NULL, NULL),
(247, 'MALUKU UTARA', 'KOTA TIDORE', 140000, NULL, NULL, NULL),
(248, 'NAD', 'KAB. ACEH BARAT', 86000, NULL, NULL, NULL),
(249, 'NAD', 'KAB. ACEH BARAT DAYA', 86000, NULL, NULL, NULL),
(250, 'NAD', 'KAB. ACEH BESAR', 108000, NULL, NULL, NULL),
(251, 'NAD', 'KAB. ACEH JAYA', 86000, NULL, NULL, NULL),
(252, 'NAD', 'KAB. ACEH SELATAN', 86000, NULL, NULL, NULL),
(253, 'NAD', 'KAB. ACEH SINGKIL', 86000, NULL, NULL, NULL),
(254, 'NAD', 'KAB. ACEH TAMIANG', 86000, NULL, NULL, NULL),
(255, 'NAD', 'KAB. ACEH TENGAH', 86000, NULL, NULL, NULL),
(256, 'NAD', 'KAB. ACEH TENGGARA', 86000, NULL, NULL, NULL),
(257, 'NAD', 'KAB. ACEH TIMUR', 86000, NULL, NULL, NULL),
(258, 'NAD', 'KAB. ACEH UTARA', 86000, NULL, NULL, NULL),
(259, 'NAD', 'KAB. BENER MERIAH', 86000, NULL, NULL, NULL),
(260, 'NAD', 'KAB. BIREUEN', 86000, NULL, NULL, NULL),
(261, 'NAD', 'KAB. GAYO LUES', 86000, NULL, NULL, NULL),
(262, 'NAD', 'KAB. NAGAN RAYA', 86000, NULL, NULL, NULL),
(263, 'NAD', 'KAB. PIDIE', 86000, NULL, NULL, NULL),
(264, 'NAD', 'KAB. PIDIE JAYA', 86000, NULL, NULL, NULL),
(265, 'NAD', 'KAB. SIMEULEU', 162000, NULL, NULL, NULL),
(266, 'NAD', 'KOTA BANDA ACEH', 47000, NULL, NULL, NULL),
(267, 'NAD', 'KOTA LANGSA', 86000, NULL, NULL, NULL),
(268, 'NAD', 'KOTA LHOKSEUMAWE', 86000, NULL, NULL, NULL),
(269, 'NAD', 'KOTA SABANG', 86000, NULL, NULL, NULL),
(270, 'NAD', 'KOTA SUBULUSSALAM', 86000, NULL, NULL, NULL),
(271, 'NTB', 'KAB. BIMA', 19000, NULL, NULL, NULL),
(272, 'NTB', 'KAB. DOMPU', 24000, NULL, NULL, NULL),
(273, 'NTB', 'KAB. LOMBOK BARAT', 19000, NULL, NULL, NULL),
(274, 'NTB', 'KAB. LOMBOK TENGAH', 19000, NULL, NULL, NULL),
(275, 'NTB', 'KAB. LOMBOK TIMUR', 19000, NULL, NULL, NULL),
(276, 'NTB', 'KAB. SUMBAWA', 19000, NULL, NULL, NULL),
(277, 'NTB', 'KAB. SUMBAWA BARAT', 19000, NULL, NULL, NULL),
(278, 'NTB', 'KOTA BIMA', 19000, NULL, NULL, NULL),
(279, 'NTB', 'KOTA MATARAM', 16000, NULL, NULL, NULL),
(280, 'NTT', 'KAB. ALOR', 70000, NULL, NULL, NULL),
(281, 'NTT', 'KAB. BELU', 70000, NULL, NULL, NULL),
(282, 'NTT', 'KAB. ENDE', 70000, NULL, NULL, NULL),
(283, 'NTT', 'KAB. FLORES TIMUR', 70000, NULL, NULL, NULL),
(284, 'NTT', 'KAB. KUPANG', 70000, NULL, NULL, NULL),
(285, 'NTT', 'KAB. LEMBATA', 70000, NULL, NULL, NULL),
(286, 'NTT', 'KAB. MANGGARAI', 70000, NULL, NULL, NULL),
(287, 'NTT', 'KAB. MANGGARAI BARAT', 70000, NULL, NULL, NULL),
(288, 'NTT', 'KAB. MANGGARAI TIMUR', 132000, NULL, NULL, NULL),
(289, 'NTT', 'KAB. NAGEKEO', 132000, NULL, NULL, NULL),
(290, 'NTT', 'KAB. NGADA', 132000, NULL, NULL, NULL),
(291, 'NTT', 'KAB. ROTE NDAO', 70000, NULL, NULL, NULL),
(292, 'NTT', 'KAB. SIKKA', 70000, 'admin@admin.com', NULL, '2015-08-09 15:04:51'),
(293, 'NTT', 'KAB. SUMBA BARAT', 70000, NULL, NULL, NULL),
(294, 'NTT', 'KAB. SUMBA BARAT DAYA', 70000, NULL, NULL, NULL),
(295, 'NTT', 'KAB. SUMBA TENGAH', 132000, NULL, NULL, NULL),
(296, 'NTT', 'KAB. SUMBA TIMUR', 70000, NULL, NULL, NULL),
(297, 'NTT', 'KAB. TIMOR TENGAH SELATAN', 70000, NULL, NULL, NULL),
(298, 'NTT', 'KAB. TIMOR TENGAH UTARA', 70000, NULL, NULL, NULL),
(299, 'NTT', 'KOTA KUPANG', 50000, NULL, NULL, NULL),
(300, 'PAPUA', 'KAB. ASMAT', 187000, NULL, NULL, NULL),
(301, 'PAPUA', 'KAB. BIAK NUMFOR', 187000, NULL, NULL, NULL),
(302, 'PAPUA', 'KAB. BOVEN DIGOEL', 187000, NULL, NULL, NULL),
(303, 'PAPUA', 'KAB. DEIYAI', 293000, NULL, NULL, NULL),
(304, 'PAPUA', 'KAB. DOGIYAI', 234000, NULL, NULL, NULL),
(305, 'PAPUA', 'KAB. INTAN JAYA', 234000, NULL, NULL, NULL),
(306, 'PAPUA', 'KAB. JAYAPURA', 91000, NULL, NULL, NULL),
(307, 'PAPUA', 'KAB. JAYAWIJAYA', 187000, NULL, NULL, NULL),
(308, 'PAPUA', 'KAB. KEEROM', 187000, NULL, NULL, NULL),
(309, 'PAPUA', 'KAB. MAMBERAMO RAYA', 351000, NULL, NULL, NULL),
(310, 'PAPUA', 'KAB. MANOKWARI', 220000, NULL, NULL, NULL),
(311, 'PAPUA', 'KAB. MAPPI', 234000, NULL, NULL, NULL),
(312, 'PAPUA', 'KAB. MERAUKE', 187000, NULL, NULL, NULL),
(313, 'PAPUA', 'KAB. MIMIKA', 133000, NULL, NULL, NULL),
(314, 'PAPUA', 'KAB. NABIRE', 110000, NULL, NULL, NULL),
(315, 'PAPUA', 'KAB. PANIAI', 234000, NULL, NULL, NULL),
(316, 'PAPUA', 'KAB. PEGUNUNGAN BINTANG', 187000, NULL, NULL, NULL),
(317, 'PAPUA', 'KAB. PUNCAK JAYA', 234000, NULL, NULL, NULL),
(318, 'PAPUA', 'KAB. SARMI', 187000, NULL, NULL, NULL),
(319, 'PAPUA', 'KAB. SUPIORI', 187000, NULL, NULL, NULL),
(320, 'PAPUA', 'KAB. TALIKORA', 187000, NULL, NULL, NULL),
(321, 'PAPUA', 'KAB. WAROPEN', 351000, NULL, NULL, NULL),
(322, 'PAPUA', 'KAB. YAHUKIMO', 351000, NULL, NULL, NULL),
(323, 'PAPUA', 'KAB. YAPEN WAROPEN', 187000, NULL, NULL, NULL),
(324, 'PAPUA', 'KOTA JAYAPURA', 91000, NULL, NULL, NULL),
(325, 'PAPUA BARAT', 'KAB. FAK-FAK', 220000, NULL, NULL, NULL),
(326, 'PAPUA BARAT', 'KAB. KAIMANA', 220000, NULL, NULL, NULL),
(327, 'PAPUA BARAT', 'KAB. RAJA AMPAT', 220000, NULL, NULL, NULL),
(328, 'PAPUA BARAT', 'KAB. SORONG', 110000, NULL, NULL, NULL),
(329, 'PAPUA BARAT', 'KAB. SORONG SELATAN', 220000, NULL, NULL, NULL),
(330, 'PAPUA BARAT', 'KAB. TELUK BINTUNI', 220000, NULL, NULL, NULL),
(331, 'PAPUA BARAT', 'KAB. TELUK WONDAMA', 220000, NULL, NULL, NULL),
(332, 'PAPUA BARAT', 'KOTA SORONG', 220000, NULL, NULL, NULL),
(333, 'RIAU', 'KAB. BENGKALIS', 52000, NULL, NULL, NULL),
(334, 'RIAU', 'KAB. INDRAGIRI HILIR', 52000, NULL, NULL, NULL),
(335, 'RIAU', 'KAB. INDRAGIRI HULU', 52000, NULL, NULL, NULL),
(336, 'RIAU', 'KAB. KAMPAR', 52000, NULL, NULL, NULL),
(337, 'RIAU', 'KAB. KUANTAN SENGINGI', 52000, NULL, NULL, NULL),
(338, 'RIAU', 'KAB. MERANTI', 65000, NULL, NULL, NULL),
(339, 'RIAU', 'KAB. PALALAWAN', 52000, NULL, NULL, NULL),
(340, 'RIAU', 'KAB. ROKAN HILIR', 52000, NULL, NULL, NULL),
(341, 'RIAU', 'KAB. ROKAN HULU', 52000, NULL, NULL, NULL),
(342, 'RIAU', 'KAB. SIAK', 52000, NULL, NULL, NULL),
(343, 'RIAU', 'KOTA DUMAI', 52000, NULL, NULL, NULL),
(344, 'RIAU', 'KOTA PEKANBARU', 39000, NULL, NULL, NULL),
(345, 'SULAWESI BARAT', 'KAB. MAJENE', 49000, NULL, NULL, NULL),
(346, 'SULAWESI BARAT', 'KAB. MAMASA', 49000, NULL, NULL, NULL),
(347, 'SULAWESI BARAT', 'KAB. MAMUJU', 49000, NULL, NULL, NULL),
(348, 'SULAWESI BARAT', 'KAB. MAMUJU UTARA', 25000, NULL, NULL, NULL),
(349, 'SULAWESI BARAT', 'KAB. POLEWALI MANDAR', 49000, NULL, NULL, NULL),
(350, 'SULAWESI SELATAN', 'KAB. BANTAENG', 49000, NULL, NULL, NULL),
(351, 'SULAWESI SELATAN', 'KAB. BARRU', 49000, NULL, NULL, NULL),
(352, 'SULAWESI SELATAN', 'KAB. BONE', 49000, NULL, NULL, NULL),
(353, 'SULAWESI SELATAN', 'KAB. BULUKUMBA', 49000, NULL, NULL, NULL),
(354, 'SULAWESI SELATAN', 'KAB. ENREKANG', 49000, NULL, NULL, NULL),
(355, 'SULAWESI SELATAN', 'KAB. GOA', 25000, NULL, NULL, NULL),
(356, 'SULAWESI SELATAN', 'KAB. JENEPONTO', 49000, NULL, NULL, NULL),
(357, 'SULAWESI SELATAN', 'KAB. LUWU TIMUR', 49000, NULL, NULL, NULL),
(358, 'SULAWESI SELATAN', 'KAB. LUWU UTARA', 49000, NULL, NULL, NULL),
(359, 'SULAWESI SELATAN', 'KAB. MAROS', 25000, NULL, NULL, NULL),
(360, 'SULAWESI SELATAN', 'KAB. PANGKAJENE KEPULAUAN', 49000, NULL, NULL, NULL),
(361, 'SULAWESI SELATAN', 'KAB. PINRANG', 49000, NULL, NULL, NULL),
(362, 'SULAWESI SELATAN', 'KAB. SELAYAR', 49000, NULL, NULL, NULL),
(363, 'SULAWESI SELATAN', 'KAB. SINDENRENG RAPPANG', 49000, NULL, NULL, NULL),
(364, 'SULAWESI SELATAN', 'KAB. SINJAI', 49000, NULL, NULL, NULL),
(365, 'SULAWESI SELATAN', 'KAB. SOPPENG', 49000, NULL, NULL, NULL),
(366, 'SULAWESI SELATAN', 'KAB. TAKALAR', 49000, NULL, NULL, NULL),
(367, 'SULAWESI SELATAN', 'KAB. TANA TORAJA', 49000, NULL, NULL, NULL),
(368, 'SULAWESI SELATAN', 'KAB. TANA TORAJA UTARA', 49000, NULL, NULL, NULL),
(369, 'SULAWESI SELATAN', 'KAB. WAJO', 49000, NULL, NULL, NULL),
(370, 'SULAWESI SELATAN', 'KOTA MAKASSAR', 25000, NULL, NULL, NULL),
(371, 'SULAWESI SELATAN', 'KOTA PALOPO', 49000, NULL, NULL, NULL),
(372, 'SULAWESI SELATAN', 'KOTA PARE-PARE', 49000, NULL, NULL, NULL),
(373, 'SULAWESI TENGAH', 'KAB. BANGGAI', 55000, NULL, NULL, NULL),
(374, 'SULAWESI TENGAH', 'KAB. BANGGAI KEPULAUAN', 55000, NULL, NULL, NULL),
(375, 'SULAWESI TENGAH', 'KAB. BUOL', 55000, NULL, NULL, NULL),
(376, 'SULAWESI TENGAH', 'KAB. DONGGALA', 42000, NULL, NULL, NULL),
(377, 'SULAWESI TENGAH', 'KAB. MOROWALI', 55000, NULL, NULL, NULL),
(378, 'SULAWESI TENGAH', 'KAB. PARIGI MOUTONG', 55000, NULL, NULL, NULL),
(379, 'SULAWESI TENGAH', 'KAB. POSO', 55000, NULL, NULL, NULL),
(380, 'SULAWESI TENGAH', 'KAB. TOJO UNA-UNA', 55000, NULL, NULL, NULL),
(381, 'SULAWESI TENGAH', 'KAB. TOLI-TOLI', 55000, NULL, NULL, NULL),
(382, 'SULAWESI TENGAH', 'KOTA PALU', 42000, NULL, NULL, NULL),
(383, 'SULAWESI TENGGARA', 'KAB. BOMBANA', 92000, NULL, NULL, NULL),
(384, 'SULAWESI TENGGARA', 'KAB. BUTON & BUTON UTARA', 73000, NULL, NULL, NULL),
(385, 'SULAWESI TENGGARA', 'KAB. KOLAKA', 73000, NULL, NULL, NULL),
(386, 'SULAWESI TENGGARA', 'KAB. KOLAKA UTARA', 73000, NULL, NULL, NULL),
(387, 'SULAWESI TENGGARA', 'KAB. KONAWE', 73000, NULL, NULL, NULL),
(388, 'SULAWESI TENGGARA', 'KAB. KONAWE UTARA/ SELATAN', 73000, NULL, NULL, NULL),
(389, 'SULAWESI TENGGARA', 'KAB. KOTA BAU-BAU', 73000, NULL, NULL, NULL),
(390, 'SULAWESI TENGGARA', 'KAB. MUNA', 73000, NULL, NULL, NULL),
(391, 'SULAWESI TENGGARA', 'KAB. WAKATOBI', 73000, NULL, NULL, NULL),
(392, 'SULAWESI TENGGARA', 'KOTA KENDARI', 50000, NULL, NULL, NULL),
(393, 'SULAWESI UTARA', 'KAB. BOLAANG MONGONDOW', 77000, NULL, NULL, NULL),
(394, 'SULAWESI UTARA', 'KAB. BOLAANG MONGONDOW UTARA', 97000, NULL, NULL, NULL),
(395, 'SULAWESI UTARA', 'KAB. KEPULAUAN SANGIHE', 77000, NULL, NULL, NULL),
(396, 'SULAWESI UTARA', 'KAB. KEPULAUAN TALAUD', 97000, NULL, NULL, NULL),
(397, 'SULAWESI UTARA', 'KAB. MINAHASA', 77000, NULL, NULL, NULL),
(398, 'SULAWESI UTARA', 'KAB. MINAHASA SELATAN', 97000, NULL, NULL, NULL),
(399, 'SULAWESI UTARA', 'KAB. MINAHASA TENGGARA', 77000, NULL, NULL, NULL),
(400, 'SULAWESI UTARA', 'KAB. MINAHASA UTARA', 97000, NULL, NULL, NULL),
(401, 'SULAWESI UTARA', 'KOTA BITUNG', 77000, NULL, NULL, NULL),
(402, 'SULAWESI UTARA', 'KOTA MANADO', 58000, NULL, NULL, NULL),
(403, 'SULAWESI UTARA', 'KOTA TOMOHON', 77000, NULL, NULL, NULL),
(404, 'SUMATERA BARAT', 'KAB. AGAM', 56000, NULL, NULL, NULL),
(405, 'SUMATERA BARAT', 'KAB. DHARMASRAYA', 56000, NULL, NULL, NULL),
(406, 'SUMATERA BARAT', 'KAB. KEPULAUAN MENTAWAI', 56000, NULL, NULL, NULL),
(407, 'SUMATERA BARAT', 'KAB. LIMA PULUH KOTA', 56000, NULL, NULL, NULL),
(408, 'SUMATERA BARAT', 'KAB. PADANG PARIAMAN', 56000, NULL, NULL, NULL),
(409, 'SUMATERA BARAT', 'KAB. PASAMAN', 56000, NULL, NULL, NULL),
(410, 'SUMATERA BARAT', 'KAB. PASAMAN BARAT', 56000, NULL, NULL, NULL),
(411, 'SUMATERA BARAT', 'KAB. PESISIR SELATAN', 56000, NULL, NULL, NULL),
(412, 'SUMATERA BARAT', 'KAB. SIJUNJUNG', 56000, NULL, NULL, NULL),
(413, 'SUMATERA BARAT', 'KAB. SOLOK', 56000, NULL, NULL, NULL),
(414, 'SUMATERA BARAT', 'KAB. SOLOK SELATAN', 56000, NULL, NULL, NULL),
(415, 'SUMATERA BARAT', 'KAB. TANAH DATAR', 56000, NULL, NULL, NULL),
(416, 'SUMATERA BARAT', 'KOTA BUKIT TINGGI', 56000, NULL, NULL, NULL),
(417, 'SUMATERA BARAT', 'KOTA PADANG', 38000, NULL, NULL, NULL),
(418, 'SUMATERA BARAT', 'KOTA PADANG PANJANG', 56000, NULL, NULL, NULL),
(419, 'SUMATERA BARAT', 'KOTA PARIAMAN', 56000, NULL, NULL, NULL),
(420, 'SUMATERA BARAT', 'KOTA PAYAKUMBUH', 56000, NULL, NULL, NULL),
(421, 'SUMATERA BARAT', 'KOTA SAWAHLUNTO', 56000, NULL, NULL, NULL),
(422, 'SUMATERA BARAT', 'KOTA SOLOK', 56000, NULL, NULL, NULL),
(423, 'SUMATERA SELATAN', 'KAB. BANYUASIN', 46000, NULL, NULL, NULL),
(424, 'SUMATERA SELATAN', 'KAB. EMPAT LAWANG', 58000, NULL, NULL, NULL),
(425, 'SUMATERA SELATAN', 'KAB. LAHAT', 46000, NULL, NULL, NULL),
(426, 'SUMATERA SELATAN', 'KAB. MUARA ENIM', 46000, NULL, NULL, NULL),
(427, 'SUMATERA SELATAN', 'KAB. MUSI BANYUASIN', 46000, NULL, NULL, NULL),
(428, 'SUMATERA SELATAN', 'KAB. MUSI RAWAS', 46000, NULL, NULL, NULL),
(429, 'SUMATERA SELATAN', 'KAB. OGAN ILIR', 46000, NULL, NULL, NULL),
(430, 'SUMATERA SELATAN', 'KAB. OGAN KOMERING ILIR', 46000, NULL, NULL, NULL),
(431, 'SUMATERA SELATAN', 'KAB. OGAN KOMERING ULU', 46000, NULL, NULL, NULL),
(432, 'SUMATERA SELATAN', 'KAB. OGAN KOMERING ULU SELATAN', 46000, NULL, NULL, NULL),
(433, 'SUMATERA SELATAN', 'KAB. OGAN KOMERING ULU TIMUR', 46000, NULL, NULL, NULL),
(434, 'SUMATERA SELATAN', 'KOTA LUBUK LINGGAU', 46000, NULL, NULL, NULL),
(435, 'SUMATERA SELATAN', 'KOTA PAGAR ALAM', 46000, NULL, NULL, NULL),
(436, 'SUMATERA SELATAN', 'KOTA PALEMBANG', 31000, NULL, NULL, NULL),
(437, 'SUMATERA SELATAN', 'KOTA PRABUMULIH', 46000, NULL, NULL, NULL),
(438, 'SUMATERA UTARA', 'KAB. ASAHAN', 55000, NULL, NULL, NULL),
(439, 'SUMATERA UTARA', 'KAB. BATUBARA', 55000, NULL, NULL, NULL),
(440, 'SUMATERA UTARA', 'KAB. DAIRI', 55000, NULL, NULL, NULL),
(441, 'SUMATERA UTARA', 'KAB. DELI SERDANG', 55000, NULL, NULL, NULL),
(442, 'SUMATERA UTARA', 'KAB. HUMBANG HASUDUTAN', 55000, NULL, NULL, NULL),
(443, 'SUMATERA UTARA', 'KAB. KARO', 55000, NULL, NULL, NULL),
(444, 'SUMATERA UTARA', 'KAB. LABUHAN BATU', 55000, NULL, NULL, NULL),
(445, 'SUMATERA UTARA', 'KAB. LANGKAT', 55000, NULL, NULL, NULL),
(446, 'SUMATERA UTARA', 'KAB. MANDAILING NATAL', 55000, NULL, NULL, NULL),
(447, 'SUMATERA UTARA', 'KAB. NIAS', 55000, NULL, NULL, NULL),
(448, 'SUMATERA UTARA', 'KAB. NIAS SELATAN', 69000, NULL, NULL, NULL),
(449, 'SUMATERA UTARA', 'KAB. PADANG LAWAS', 55000, NULL, NULL, NULL),
(450, 'SUMATERA UTARA', 'KAB. PADANG LAWAS UTARA', 55000, NULL, NULL, NULL),
(451, 'SUMATERA UTARA', 'KAB. PAKPAK BARAT', 55000, NULL, NULL, NULL),
(452, 'SUMATERA UTARA', 'KAB. SAMOSIR', 55000, NULL, NULL, NULL),
(453, 'SUMATERA UTARA', 'KAB. SERDANG BEDAGAI', 55000, NULL, NULL, NULL),
(454, 'SUMATERA UTARA', 'KAB. SIMALUNGUN', 55000, NULL, NULL, NULL),
(455, 'SUMATERA UTARA', 'KAB. TAPANULI SELATAN', 55000, NULL, NULL, NULL),
(456, 'SUMATERA UTARA', 'KAB. TAPANULI TENGAH', 55000, NULL, NULL, NULL),
(457, 'SUMATERA UTARA', 'KAB. TAPANULI UTARA', 55000, NULL, NULL, NULL),
(458, 'SUMATERA UTARA', 'KAB. TEBING TINGGI', 69000, NULL, NULL, NULL),
(459, 'SUMATERA UTARA', 'KAB. TOBA SAMOSIR', 55000, NULL, NULL, NULL),
(460, 'SUMATERA UTARA', 'KOTA BINJAI', 55000, NULL, NULL, NULL),
(461, 'SUMATERA UTARA', 'KOTA MEDAN', 39000, NULL, NULL, NULL),
(462, 'SUMATERA UTARA', 'KOTA PADANG SIDEMPUAN', 55000, NULL, NULL, NULL),
(463, 'SUMATERA UTARA', 'KOTA PEMATANGSIANTAR', 55000, NULL, NULL, NULL),
(464, 'SUMATERA UTARA', 'KOTA SIBOLGA', 55000, NULL, NULL, NULL),
(465, 'SUMATERA UTARA', 'KOTA TANJUNG BALAI', 55000, NULL, NULL, NULL),
(466, 'SUMATERA UTARA', 'KOTA TEBING TINGGI', 55000, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_address` text NOT NULL,
  `user_state` varchar(50) NOT NULL,
  `user_city` varchar(50) NOT NULL,
  `user_zipcode` int(11) NOT NULL,
  PRIMARY KEY (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_email`, `user_password`, `user_name`, `user_address`, `user_state`, `user_city`, `user_zipcode`) VALUES
('mbahsomo@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'mbahsomo', 'sda', 'DKI JAKARTA', 'DKI JAKARTA', 1234);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `fk_cart_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_users1` FOREIGN KEY (`user_email`) REFERENCES `user` (`user_email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `fk_order_detail_order1` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_detail_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `fk_payment_order1` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_product_category` FOREIGN KEY (`category_id`) REFERENCES `product_category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `fk_product_images_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
SET FOREIGN_KEY_CHECKS = 0;
