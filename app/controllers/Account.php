<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {

	function __construct(){
        parent::__construct();
	}

	public function index()
	{
		$data['isi'] = $this->load->view('frontend/account',null,true);
		$this->load->view('frontend/template/index',$data);
	}

	public function transaksi()
	{
		$data['isi'] = $this->load->view('frontend/account_transksi',null,true);
		$this->load->view('frontend/template/index',$data);	
	}

	public function pembayaran()
	{
		$data['isi'] = $this->load->view('frontend/account_bayar',null,true);
		$this->load->view('frontend/template/index',$data);	
	}

}

/* End of file account.php */
/* Location: ./application/controllers/account.php */