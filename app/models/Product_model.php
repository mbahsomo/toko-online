<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * Kasus Create	: mbahsomo
 * Nama File	: product_model.php
 */
class Product_model extends T_Model {

	private $field = array();

    function __construct() {
        parent::__construct();
        $this->set_table('product');
        $this->set_key_field( 'product_id' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();
        
        for ($i=0; $i < count($this->field) ; $i++) { 
            $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $fields['user_entry'] = $this->session->userdata('user_name');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }
    
    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->delete_data();
    }

    public function get_rule($insert = true) {
        $rl =  array(
            array(
                'field' => 'category_id',
                'label' => 'Kategori',
                'fieldLabel' => 'category_name',
                'width' => 200,
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'product_name',
                'label' => 'Nama',
                'width' => 200,
                'rules' => 'xss_clean|max_length[25]|required'
            ),array(
                'field' => 'product_desc',
                'label' => 'Keterangan',
                'width' => 200,
                'rules' => 'xss_clean'
            ),array(
                'field' => 'product_price',
                'label' => 'Harga',
                'width' => 200,
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'product_qty',
                'label' => 'Stok',
                'width' => 200,
                'rules' => 'xss_clean|numeric|required'
            )
        );
        if (!$insert) {
            return array_merge(
                array(
                    array(
                        'field' => 'product_id',
                        'label' => 'ID',
                        'rules' => 'required|numeric|required'
                    )
                ), $rl
            );
        } else {
            return $rl;
        }
    }
    
    public function search($field='u_fname', $value='%', $start=0, $stop=100, $like = true){
        $this->set_null();
        $this->set_fields( $this->get_table() . '.*, category_name');
        $this->set_join(array(
            array(
                'TABLE' => 'product_category',
                'FIELD' => 'product_category.category_id=' . $this->get_table() . '.category_id',
                'JOIN'  => 'inner'
            )
        ));
        $this->set_start($start);
        $this->set_stop($stop);
        $fieldv = explode(";", $field);
        $valuev = explode(";", $value);
        if (count($valuev) > 0) {
            for ($a = 0; $a < count($valuev); $a++) {
                if ($valuev[$a] !== '') {
                    if($like){
                        $this->set_like(array(
                            $this->get_table() . '.'.$fieldv[$a] => $valuev[$a]
                        ));
                    }else{
                        $this->set_params(array(
                            $this->get_table() . '.'.$fieldv[$a] => $valuev[$a]
                        ));
                    }
                }
            }
        }
        $this->set_orderby('date_edit desc');
        $this->load->model('Product_images_model');
        $data = array();
        foreach ($this->get_data() as $key => $value) {
            $this->Product_images_model->set_fields('pi_image');
            $this->Product_images_model->set_params(array('product_id'=>$value['product_id']));
            $this->Product_images_model->set_start(0);
            $this->Product_images_model->set_stop(1);
            $gmb = $this->Product_images_model->get_data();
            $value['image'] = (count($gmb)>0)?$gmb[0]['pi_image']:'no-image.jpg';
            $data[] = $value;
        }
        return $data;
    }

    public function detail($pid){
        $this->set_null();
        $this->set_fields( $this->get_table() . '.*, category_name');
        $this->set_join(array(
            array(
                'TABLE' => 'product_category',
                'FIELD' => 'product_category.category_id=' . $this->get_table() . '.category_id',
                'JOIN'  => 'inner'
            )
        ));
        $this->set_start(0);
        $this->set_stop(1);
        $this->set_params(array(
            $this->get_table() . '.product_id' => $pid
        ));
        $this->load->model('Product_images_model');
        $this->Product_images_model->set_fields('pi_image');
        $this->Product_images_model->set_params(array('product_id'=>$pid));
        $this->Product_images_model->set_start(0);
        $this->Product_images_model->set_stop(100);
        $gmb = $this->Product_images_model->get_data();
        $data = array();
        foreach ($this->get_data() as $key => $value) {
            if(count($gmb)<=0){
                $gmb[0]['pi_image'] = 'no-image.jpg';
            }
            $value['images'] = $gmb ;
            $data[] = $value;
        }
        return $data;
    }
        

}

/* End of file product_model.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/sanmar/app/models/product_model.php */