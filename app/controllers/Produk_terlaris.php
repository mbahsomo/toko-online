<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: produk_terlaris.php
 */
class Produk_terlaris extends T_Controller {

	public function index()
	{
		$this->load->model('Product_categori_model');
		$this->load->model('Product_model','mdl');
		$data1['cat'] = $this->Product_categori_model->get_all();

		$this->mdl->set_null();
		$this->mdl->set_start(0);
        $this->mdl->set_stop(6);
		$this->mdl->set_fields('product.*, (select count(product_id) from order_detail where order_detail.product_id=product.product_id) as jual');
		$this->mdl->set_orderby('jual desc');

		$data = array();
		$this->load->model('Product_images_model');
        foreach ($this->mdl->get_data() as $key => $value) {
            $this->Product_images_model->set_fields('pi_image');
            $this->Product_images_model->set_params(array('product_id'=>$value['product_id']));
            $this->Product_images_model->set_start(0);
            $this->Product_images_model->set_stop(1);
            $gmb = $this->Product_images_model->get_data();
            $value['image'] = (count($gmb)>0)?$gmb[0]['pi_image']:'no-image.jpg';
            $data[] = $value;
        }
        

		$data1['barang'] = $data;
		$data2['isi'] = $this->load->view('frontend/index',$data1,true);
		$this->load->view('frontend/template/index',$data2);
	}

}

/* End of file produk_terlaris.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/thera/app/controllers/produk_terlaris.php */