var myApp = angular.module('dDate',[]);

myApp.directive('dDate', function () {
    return {
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(el).datepicker({
                format: "dd-mm-yyyy",
			    todayBtn: "linked",
			    clearBtn: true,
			    language: "id",
			    autoclose: true,
			    todayHighlight: true,
			    beforeShowDay: function (date){
			    	if (date.getMonth() == (new Date()).getMonth())
				        switch (date.getDate()){
				          case 4:
				            return {
				              tooltip: 'Example tooltip',
				              classes: 'active'
				            };
				          case 8:
				            return false;
				          case 12:
				            return "green";
				        }
			    }
            });
            //console.log(ngModel);
            //$(el).datepicker('update',ngModel);
        }
    };
});

myApp.directive('dDateMask', function () {
    return {
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
	 		$(el).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
        }
    };
});