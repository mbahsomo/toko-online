<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create  : mbahsomo
 * Nama File    : Order.php
 */
class Order extends T_Controller {

    var $stop = 0;
    var $menudata = array();

    function __construct() {
        parent::__construct();
        $this->load->model('Order_model', 'mdl');
        $this->load->model('Order_detail_model', 'mdldetail');
        $this->stop = BATAS_REC;
    }

    public function access_rules() {
        return array(
            array('allow',
                'actions' => array('cetak','load_view','load_controller','index','insert_data','edit_data','delete_data','search','get_newcode','get_menu','edit_menu','export_xls','get_all','getdetail','get_order','batal','search_detail','get_harga'),
                'expression' => $this->session->userdata('login')
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }

    public function load_view(){
        $data ['evt_view'] = $this->get_akses( $this->cid, 'v');
        $data ['evt_add'] = $this->get_akses( $this->cid, 'a');
        $data ['evt_edit'] = $this->get_akses( $this->cid, 'e');
        $data ['evt_delete'] = $this->get_akses( $this->cid, 'd');
        $data ['evt_print'] = $this->get_akses( $this->cid, 'p');
        $this->load->view( $this->cid . '/view');

    }

    public function load_controller(){
        $this->load->helper('controller_helper');
        $this->load->library('Doevent');
        $doe = new Doevent();
        $doe->hashkey();
        $data['stop'] = $this->stop;
        $data['controller'] =  ucfirst($this->cid) .'Controller';
        $doe->load_view( $this->cid  .'/controller', $data, '<span><div>');
    }

    public function insert_data() {
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            //echo $error;
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => $this->mdl->insert(), "kode" => $this->mdl->get_rec_id())));
        }
    }

    public function edit_data() {
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => $this->mdl->update($this->input->post($this->mdl->get_key_field(), true)))));
        }
    }

    public function delete_data() {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                                array(
                                    'success' => $this->mdl->delete($this->input->post($this->mdl->get_key_field(), true)),
                                    'max_page' => $this->get_max_page($this->mdl->get_tot_rows(), $this->stop)
                                )
        ));
    }

    public function search() {
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(
            array(
                'success' => true,
                'rec' => $this->mdl->search(
                        $this->input->post('field', true), $this->input->post('value', true), $this->input->post('stop', true), $this->input->post('limit', true)
                ),
                'max_page' => $this->mdl->get_tot_rows()
            )
        ));
    }


    public function getdetail() {
        $this->load->model('Order_detail_model');
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(
            array(
                'success' => true,
                'rec' => $this->Order_detail_model->search(
                        $this->input->post('kode', true)
                ),
                'max_page' => $this->Order_detail_model->get_tot_rows()
            )
        ));
    }

    public function get_all() {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    array(
                        'rec' => $this->mdl->get_all()
                    )
        ));
    }
    public function get_order() {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    array(
                        'rec' => $this->mdl->get_all('order_id, order_total, order_shipprice ',"order_status = 'O'")
                    )
        ));
    }

    public function batal()
    {
        $this->mdl->set_null();
        $this->mdl->set_fields(array('order_status' => 'C'));
        $this->mdl->set_params(array('order_id' => $this->input->post('kode',true)));
        $sukses = $this->mdl->update_data();
        if($sukses)
            $this->_update_stok($this->input->post('kode',true));
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                array(
                    'success' => $sukses
                )
            ));
    }

    private function _update_stok($id)
    {
        $this->load->model('Order_detail_model');
        $this->Order_detail_model->set_null();
        $this->Order_detail_model->set_fields('product_id, orderd_qty');
        $this->Order_detail_model->set_params(array('order_id'=>$id));
        foreach ($this->Order_detail_model->get_data() as $key => $value) {
            $this->Order_detail_model->set_null();
            $this->Order_detail_model->exec_query("update product set product_qty = product_qty+" . $value['orderd_qty'] ." where product_id=" . $value['product_id'], false);
        }
    }

    public function search_detail()
    {
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(
            array(
                'rec' => $this->mdldetail->search($this->input->post('order_id',true))
            )
        ));
        
    }

    public function get_harga()
    {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    array(
                        'rec' => $this->mdl->get_all('order_id, order_total, order_shipprice ',"order_id =" . $this->input->post('order_id',true))
                    )
        ));
    }
}

/* End of file Order.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/thera/app/controllers/Order.php */