<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: Register.php
 */
class Register extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('User_model','mdl');
	}

	public function index()
	{
			
	}

	public function login(){
        $success = false;
        $pesan = "";
		$this->mdl->set_params( array('user_email'=>$this->input->post('user_email',true)) );
        $rec = $this->mdl->get_data();
        if( count($rec)>0){        	
        	if ($rec[0]['user_password']!== md5($this->input->post('user_password',true)) ){
        		$pesan = 'Password Salah ' ;//. $rec[0]['user_password'] . ' = ' . md5($this->input->post('user_password',true));
        	}else{
                $success = true;
                if($this->input->post('ingat',true)){
                    $data['new_expiration'] = 0;
                    $this->session->sess_expiration = $data['new_expiration'];
                }else{
                    $data['new_expiration'] = 60*60*24*30;
                    $this->session->sess_expiration = $data['new_expiration'];
                }              
                
                //Ambil toko
                $session['login'] = 1;
                $session['name'] = $rec[0]['user_email'];
                $this->session->set_userdata($session);                
        	}
        	
        }else{
        	$pesan = 'username Tidak terdaftar';
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(
                json_encode(
                    array('success' => $success, 
                        "msg"=> $pesan ,
                        'url' => site_url('main')
                    )
                )
            );
	}

	public function newuser()
	{
		//$_POST['user_password'] = md5($_POST['user_password']);
		$_POST['user_zipcode'] = '1234';
		$this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => $this->mdl->insert(), "kode" => $this->mdl->get_rec_id())));
        }
	}

}

/* End of file Register.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/thera/app/controllers/Register.php */