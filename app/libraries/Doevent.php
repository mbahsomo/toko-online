<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: Doevent.php
 */
class Doevent {

    function angka2text($text, $panjang) {
        $hasil = '';
        for ($a = 0; $a < $panjang - strlen($text); $a++) {
            $hasil.='0';
        }
        $hasil.=$text;
        return $hasil;
    }

    function get_nm_bulan($bil) {
        $arrBulan = $this->set_nm_bulan();
        return $arrBulan[ intval($bil)];
    }

    function set_nm_bulan() {
        return array(
            1 => 'JANUARI',
            2 => 'FEBRUARI',
            3 => 'MARET',
            4 => 'APRIL',
            5 => 'MEI',
            6 => 'JUNI',
            7 => 'JULI',
            8 => 'AGUSTUS',
            9 => 'SEPTEMBER',
            10 => 'OKTOBER',
            11 => 'NOVEMBER',
            12 => 'DESEMBER'
        );
    }

    function load_view($view, $data = array(), $allowed="") {
        $CI = & get_instance();
        $html = $CI->load->view($view, $data, true);
        echo strip_tags($html,$allowed);
        exit();
    }
    
    public function hashkey() {
        $this->CI = & get_instance();
        $this->CI->load->helper('security');
        $this->CI->session->set_userdata('hashkey', do_hash($this->CI->session->userdata('session_id') . time(), 'md5'));
    }
    
    public function rpt2xls($html, $nmfl='export.xls'){
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=".$nmfl);
        header("Pragma: no-cache");
        header("Expires: 0");
        $rpt = explode("<!--detail-->", $html);
        echo $rpt[1];
        exit();
    }

    public function str_to_int($str){
        return str_replace(",","",$str);
    }

    public function set_terbilang($x){
        $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        if ($x < 12)
            return " " . $abil[$x];
        elseif ($x < 20)
            return $this->set_terbilang($x - 10) . " belas";
        elseif ($x < 100)
            return $this->set_terbilang($x / 10) . " puluh" . $this->set_terbilang($x % 10);
        elseif ($x < 200)
            return " seratus" . $this->set_terbilang($x - 100);
        elseif ($x < 1000)
            return $this->set_terbilang($x / 100) . " ratus" . $this->set_terbilang($x % 100);
        elseif ($x < 2000)
            return " seribu" . $this->set_terbilang($x - 1000);
        elseif ($x < 1000000)
            return $this->set_terbilang($x / 1000) . " ribu" . $this->set_terbilang($x % 1000);
        elseif ($x < 1000000000)
            return $this->set_terbilang($x / 1000000) . " juta" . $this->set_terbilang($x % 1000000);
    }

    public function set_romawi($x){
        $abil = array("", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X");
        if ($x < 11)
            return " " . $abil[$x];
        elseif ($x < 20)
            return $this->set_romawi($x - 10) ;
        elseif ($x < 100)
            return $this->set_romawi($x / 10)  . $this->set_romawi($x % 10);
        elseif ($x < 200)
            return  $this->set_romawi($x - 100);
        elseif ($x < 1000)
            return $this->set_romawi($x / 100) . $this->set_romawi($x % 100);
        elseif ($x < 2000)
            return  $this->set_romawi($x - 1000);
        elseif ($x < 1000000)
            return $this->set_romawi($x / 1000)  . $this->set_romawi($x % 1000);
        elseif ($x < 1000000000)
            return $this->set_romawi($x / 1000000)  . $this->set_romawi($x % 1000000);
    }

    public function ping_domain($domain){
        /*$starttime = microtime(true);
        $file      = fsockopen ($domain, 80, $errno, $errstr, 10);
        $stoptime  = microtime(true);
        $status    = 0;
     
        if (!$file) {
            $status = -1; 
        } else {
            fclose($file);
            $status = ($stoptime - $starttime) * 1000;
            $status = floor($status);
        }*/
        $status    = 0;   
        $ip = gethostbyname($domain);
        if ($ip!==$domain){
            $status=1;
        }
        return $status;        
    }

    public function to_num($num){
        $huruf = array( 'a', 'b', 'c', 'd', 'e',
                       'f', 'g', 'h', 'i', 'j',
                       'k', 'l', 'm', 'n', 'o',
                       'p', 'q', 'r', 's', 't',
                       'u', 'v', 'w', 'x', 'y',
                       'z'
                       );
        return  $huruf[$num-1];
    }

    function code128BarCode ( $code , $density = 1 ) {
        
        //Creates an array for alphanumeric codes
        //Formatted as numerical representations of "B S B S B S", where B is the number of lines and S is the number of spaces
        
        $code128_bar_codes      =   array(
                                    212222, 222122, 222221, 121223, 121322, 131222, 122213, 122312, 132212, 221213, 221312, 231212, 112232, 122132, 122231, 113222, 123122, 123221, 223211, 221132, 221231,
                                    213212, 223112, 312131, 311222, 321122, 321221, 312212, 322112, 322211, 212123, 212321, 232121, 111323, 131123, 131321, 112313, 132113, 132311, 211313, 231113, 231311,
                                    112133, 112331, 132131, 113123, 113321, 133121, 313121, 211331, 231131, 213113, 213311, 213131, 311123, 311321, 331121, 312113, 312311, 332111, 314111, 221411, 431111,
                                    111224, 111422, 121124, 121421, 141122, 141221, 112214, 112412, 122114, 122411, 142112, 142211, 241211, 221114, 413111, 241112, 134111, 111242, 121142, 121241, 114212,
                                    124112, 124211, 411212, 421112, 421211, 212141, 214121, 412121, 111143, 111341, 131141, 114113, 114311, 411113, 411311, 113141, 114131, 311141, 411131, 211412, 211214,
                                    211232, 23311120
                                );
        
        //Get the width and height of the barcode
        //Determine the height of the barcode, which is >= .5 inches
        
        $width          =   (((11 * strlen($code)) + 35) * ($density/72)); // density/72 determines bar width at image DPI of 72
        $height         =   ($width * .15 > .7) ? $width * .15 : .7;
        
        $px_width       =   round($width * 72);
        $px_height      =   ($height * 72);
        
        //Create a true color image at the specified height and width
        //Allocate white and black colors
        
        $img        =   imagecreatetruecolor($px_width, $px_height);
        $white      =   imagecolorallocate($img, 255, 255, 255);
        $black      =   imagecolorallocate($img, 0, 0, 0);
        
        //Fill the image white
        //Set the line thickness (based on $density)
        
        imagefill($img, 0, 0, $white);
        imagesetthickness($img, $density);
        
        //Create the checksum integer and the encoding array
        //Both will be assembled in the loop
        
        $checksum   =   CODE128B_START_BASE;
        $encoding   =   array($code128_bar_codes[CODE128B_START_BASE]);
        
        //Add Code 128 values from ASCII values found in $code
        
        for($i = 0; $i < strlen($code); $i++) {
            
            //Add checksum value of character
            
            $checksum   +=  (ord(substr($code, $i, 1)) - 32) * ($i + 1);
            
            //Add Code 128 values from ASCII values found in $code
            //Position is array is ASCII - 32
            
            array_push($encoding, $code128_bar_codes[(ord(substr($code, $i, 1))) - 32]);
            
        }
        
        //Insert the checksum character (remainder of $checksum/103) and STOP value
                
        array_push($encoding, $code128_bar_codes[$checksum%103]);
        array_push($encoding, $code128_bar_codes[STOP]);
        
        //Implode the array as string
        
        $enc_str    =   implode($encoding);
        
        //Assemble the barcode
        
        for($i = 0, $x = 0, $inc = round(($density/72) * 100); $i < strlen($enc_str); $i++) {
            
            //Get the integer value of the string element
            
            $val    =   intval(substr($enc_str, $i, 1));
            
            //Create lines/spaces
            //Bars are generated on even sequences, spaces on odd
            
            for($n = 0; $n < $val; $n++, $x+=$inc) { if($i%2 == 0) imageline($img, $x, 0, $x, $px_height, $black); }
            
        }
        
        //Return the image
        
        return $img;
        
    }
    
    public function page_break($head, $tclass="detail"){
        echo '</tbody></table>';
        echo '<div class="newpage"></div>';
        echo '<table class="' .$tclass.'">' . $head. '<tbody>';
    }
}
