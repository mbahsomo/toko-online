function cssfile(url_css){
    return $("<link rel='stylesheet' type='text/css' href='"+url_css+"'>");
}
$("head").append(cssfile(base_url + 'assets/lib/abn-tree/abn_tree.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/splitter/splitter.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/bootstrap/css/bootstrap.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/bootstrap/css/dropdowns-enhancement.css'));
$("head").append(cssfile(base_url + 'assets/lib/font-awesome/css/font-awesome.min.css'));
//$("head").append(cssfile(base_url + 'assets/css/dashboard.css'));
$("head").append(cssfile(base_url + 'assets/css/t-table.css'));
$("head").append(cssfile(base_url + 'assets/css/style.css'));
$("head").append(cssfile(base_url + 'assets/css/bootstrap-theme.css'));

$("head").append(cssfile(base_url + 'assets/lib/loading/loading-bar.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/ng-grid/ng-grid.min.css'));
$("head").append(cssfile(base_url + 'assets/css/ng-grid-theme-biru.css'));
$("head").append(cssfile(base_url + 'assets/lib/toaster/toaster.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/date-picker/date.css'));
$("head").append(cssfile(base_url + 'assets/lib/sweet-alert/sweet-alert.css'));
$("head").append(cssfile(base_url + 'assets/lib/ag-grid/angular-grid.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/ag-grid/theme-fresh.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/ag-grid/theme-dark.min.css'));
$script(
        [
            base_url + 'assets/lib/jquery/jquery-ui.min.js',
            base_url + 'assets/lib/splitter/splitter.min.js',
            base_url + 'assets/lib/bootstrap/js/bootstrap.min.js',
            base_url + 'assets/lib/bootstrap/js/dropdowns-enhancement.js',
            base_url + 'assets/lib/ng-grid/ng-grid.min.js',
            base_url + 'assets/lib/angularjs/angular-route.min.js',
            base_url + 'assets/lib/angularjs/angular-animate.min.js',
            base_url + 'assets/lib/angularjs/angular-sanitize.js',
            base_url + 'assets/lib/loading/loading-bar.min.js',
            base_url + 'assets/lib/toaster/toaster.min.js',
            base_url + 'assets/js/price-format.js',
            base_url + 'assets/lib/doevent/doevent-print.js',
            base_url + 'assets/lib/doevent/doevent-tools.js',
            base_url + 'assets/lib/doevent/doevent-upload.js',
            base_url + 'assets/lib/doevent/doevent-input.js',
            base_url + 'assets/lib/doeventgrid/js/doevent-grid.js',
            base_url + 'assets/lib/abn-tree/abn_tree_directive.min.js',
            base_url + 'assets/lib/angularjs/mask.min.js',
            base_url + 'assets/js/md5.min.js',
            base_url + 'assets/lib/auto-complite/ui-bootstrap-tpls-0.9.0.min.js',
            base_url + 'assets/lib/jquery/jquery.maskedinput.min.js',
            base_url + 'assets/lib/date-picker/date.min.js',
            base_url + 'assets/lib/date-picker/m-date.js',
            base_url + 'assets/lib/date-picker/locales/bootstrap-datepicker.id.js',            
            base_url + 'assets/lib/blog-ui/angular-block-ui.min.js',            
            base_url + 'assets/lib/sweet-alert/sweet-alert.min.js',
            base_url + 'assets/lib/paging/paging.js',
            base_url + 'assets/lib/ag-grid/angular-grid.min.js'
        ],
        'app'
        );

var bahasa = [
    {"bahasa":"ina","isi":[]}
];



$script.ready('app', function() {

    /*
    Untuk splitter
    */

    $().ready(function() {
        $("#splitterContainer").splitter({minAsize:100,maxAsize:400,splitVertical:true,A:$('#leftPane'),B:$('#rightPane'),slave:$("#rightSplitterContainer"),closeableto:0});
        
    });

    var app = angular.module('DoEventApp', ['ngRoute','angularBootstrapNavTree','ngGrid','chieffancypants.loadingBar','toaster','dFile','ui.mask','ui.bootstrap','dDate','dTools','deFocus','blockUI','ui.bootstrap','dGrid','ngSanitize',"angularGrid","brantwills.paging"]);

    app.filter('dateEntry', function myDateFormat($filter) {
        return function(text) {
            if (text !== null) {
                var tempdate = new Date(text.replace(/-/g, "/"));
                return $filter('date')(tempdate, "dd-MM-yyyy HH:mm:ss");
            } else {
                return $filter('date')(new Date(), "dd-MM-yyyy HH:mm:ss");
            }
        };
    });
    app.provider('jsDeps', function() {
        this.$get = function(dependencies) {
            return {jsdeps: function($q, $rootScope) {
                    var deferred = $q.defer();
                    $script(dependencies, function() {
                        $rootScope.$apply(function() {
                            deferred.resolve();
                        });
                    });
                    return deferred.promise;
                }};
        };
    });


    app.config(function($routeProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, jsDepsProvider, $httpProvider)
    {
        app.controllerProvider = $controllerProvider;
        app.compileProvider = $compileProvider;
        app.routeProvider = $routeProvider;
        app.filterProvider = $filterProvider;
        app.provide = $provide;
        
        /*Master data*/
        $routeProvider.when('/', {
            templateUrl: site_url + 'awal/load_view',
            title: 'Awal',
            controller: 'AwalController',
            resolve: jsDepsProvider.$get([site_url + 'awal/load_controller'])
        }).when('/kategori', {
            templateUrl: site_url + 'kategori/load_view',
            controller: 'KategoriController',
            resolve: jsDepsProvider.$get([site_url + 'kategori/load_controller'])
        }).when('/shipping_price', {
            templateUrl: site_url + 'shipping_price/load_view',
            controller: 'Shipping_priceController',
            resolve: jsDepsProvider.$get([site_url + 'shipping_price/load_controller'])
        }).when('/product', {
            templateUrl: site_url + 'product/load_view',
            controller: 'ProductController',
            resolve: jsDepsProvider.$get([site_url + 'product/load_controller'])
        }).when('/order', {
            templateUrl: site_url + 'order/load_view',
            controller: 'OrderController',
            resolve: jsDepsProvider.$get([site_url + 'order/load_controller'])
        }).when('/lap-stok', {
            templateUrl: site_url + 'lap_stok/load_view',
            controller: 'Lap_stokController',
            resolve: jsDepsProvider.$get([site_url + 'lap_stok/load_controller'])
        });

    });

    app.directive('shortcut', function() {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            link:    function postLink(scope, iElement, iAttrs){
                jQuery(document).on('keyup', function(e){
                    scope.$apply(scope.keyUp(e));
                });
            }
        };
    });

    app.controller('MenuCtrl', function($scope, $timeout, $location, $http, $filter) {
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.show_reload = false; 
        $scope.my_tree_handler = function(branch) {
            var _ref;
            //console.log(branch);
            if (branch.url !== undefined) {
                $location.path(branch.url);
            }
            $scope.output = "You selected: " + branch.label;
        };
        
        treedata_avm = [
            {
                label : 'Master',
                children : [
                    
                    {
                        label : 'Kategori',
                        url : 'kategori'
                    },{
                        label : 'Shipping Price',
                        url : 'shipping_price'
                    },{
                        label : 'Product',
                        url : 'product'
                    }   
                ]
            },{
                label : 'Transaksi',
                children : [
                    
                    {
                        label : 'Order',
                        url : 'order'
                    }
                ]
            },{
                label : 'Laporan',
                children : [
                    
                    {
                        label : 'Lap.Stok Barang',
                        url : 'lap-stok'
                    }
                    
                ]
            }                
        ];
        $scope.my_data = treedata_avm;
        $scope.my_tree = tree = {};

        $scope.keyUp = function(e) {
            /*var obj = e.currentTarget.activeElement.localName ;
            if(obj ==='body'){
                switch (e.which){
                    case 37 :
                        $scope.my_tree.collapse_branch();
                        break;
                    case 38 :
                        $scope.my_tree.select_prev_branch();
                        break;
                    case 39 :
                        $scope.my_tree.expand_branch();
                        break;
                    case 40 :
                        $scope.my_tree.select_next_branch();
                        break;
                }
            }*/
        };      
        
        $scope.reloadMenu = function() {
            $scope.my_data = [];
            $scope.doing_async = true;
            return $timeout(function() {
                if (Math.random() < 0.5) {
                    $scope.my_data = treedata_avm;
                } else {
                    $scope.my_data = treedata_geography;
                }
                $scope.doing_async = false;
                $scope.my_tree.expand_all();
            }, 1000);
            $scope.my_tree.expand_all();
        };
       
        $http.get( base_url + 'assets/app/lang/'+ setlang).success(function(response) {
            bahasa[0].isi.push(response);
        });

    });

    angular.bootstrap(document.body, ['DoEventApp']);

});