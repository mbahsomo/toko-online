<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam Aplikasi ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: Lap_stok.php
 */
class Lap_stok extends T_Controller {

	var $stop = 0;
    var $menudata = array();

    function __construct() {
        parent::__construct();
        $this->load->model('Product_model', 'mdl');
        $this->stop = BATAS_REC;
    }

    public function access_rules() {
        return array(
            array('allow',
                'actions' => array('cetak','load_view','load_controller','index', 'search'),
                'expression' => $this->session->userdata('login')
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }

    public function load_view(){
        $this->load->view( $this->cid . '/view');
    }

    public function load_controller(){
        $this->load->helper('controller_helper');
        $this->load->library('Doevent');
        $doe = new Doevent();
        $doe->hashkey();
        $data['stop'] = $this->stop;
        $data['controller'] =  ucfirst($this->cid) .'Controller';
        $doe->load_view( $this->cid  .'/controller', $data, '<span><div>');
    }

}

/* End of file Lap_stok.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/thera/app/controllers/Lap_stok.php */