<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: user_model.php
 */
class User_model extends T_Model {

	private $field = array();

    function __construct() {
        parent::__construct();
        $this->set_table('user');
        $this->set_key_field( 'user_email' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();
        
        for ($i=0; $i < count($this->field) ; $i++) { 
            if($this->field[$i]=='user_password'){
                if($this->input->post($this->field[$i] , true)!==''){
                    $fields[$this->field[$i]] = md5($this->input->post($this->field[$i] , true));
                }
            }else{
                $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
            }
        }
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }
    
    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function get_rule($insert = true) {
        $rl =  array(
            array(
                'field' => 'user_email',
                'label' => 'User Name',
                'width' => '200',
                'rules' => 'xss_clean|max_length[50]|required'
            ),array(
                'field' => 'user_password',
                'label' => 'Password',
                'width' => '300',
                'rules' => 'xss_clean'
            ),array(
                'field' => 'user_name',
                'label' => 'Nama User',
                'rules' => 'xss_clean|max_length[25]'
            ),array(
                'field' => 'user_address',
                'label' => 'Alamat',
                'grid'  => false,
                'rules' => 'xss_clean'
            ),array(
                'field' => 'user_state',
                'label' => 'Propinsi',
                'grid'  => false,
                'rules' => 'xss_clean'
            ),array(
                'field' => 'user_city',
                'label' => 'Kota',
                'grid'  => false,
                'rules' => 'xss_clean'
            ),array(
                'field' => 'user_zipcode',
                'label' => 'Kode Post',
                'grid'  => false,
                'rules' => 'xss_clean'
            )
        );
        return $rl;
    }
    
    public function search($field='u_fname', $value='%', $start=0, $stop=5){
        $this->set_null();
        $this->set_fields( $this->get_table() . '.*');
        $this->set_start($start);
        $this->set_stop($stop);
        $fieldv = explode(";", $field);
        $valuev = explode(";", $value);
        if (count($valuev) > 0) {
            for ($a = 0; $a < count($valuev); $a++) {
                if ($valuev[$a] !== '') {
                    $this->set_like(array(
                        $this->get_table() . '.'.$fieldv[$a] => $valuev[$a]
                    ));
                }
            }
        }
        return $this->get_data();
    }
    
    
}

/* End of file user_model.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/sanmar/app/models/user_model.php */