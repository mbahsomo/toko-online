<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * Atribut_kriteria Create	: mbahsomo
 * Nama File	: atribut_kriteria_model.php
 */
class Atribut_kriteria_model extends T_Model {

	private $field = array();

    function __construct() {
        parent::__construct();
        $this->set_table('atribut_kriteria');
        $this->set_key_field( 'ak_id' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();
        
        for ($i=0; $i < count($this->field) ; $i++) { 
            $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $fields['user_entry'] = $this->session->userdata('user_name');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }
    
    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function get_rule($insert = true) {
        $rl =  array(
            array(
                'field' => 'at_id',
                'label' => 'Nama Atribut',
                'fieldLabel' => 'at_name',
                'width' => 200,
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'ak_kriteria',
                'label' => 'Kriteria',
                'width' => 300,
                'rules' => 'xss_clean|max_length[45]'
            )
        );
        if (!$insert) {
            return array_merge(
                array(
                    array(
                        'field' => 'ak_id',
                        'label' => 'ID',
                        'rules' => 'required|numeric|required'
                    )
                ), $rl
            );
        } else {
            return $rl;
        }
    }
    
    public function search($field='u_fname', $value='%', $start=0, $stop=5){
        $this->set_null();
        $this->set_fields( $this->get_table() . '.*, at_name');
        $this->set_join(array(
            array(
                'TABLE' => 'atribut',
                'FIELD' => 'atribut.at_id=' . $this->get_table() . ".at_id",
                'JOIN' => 'inner'
            )
        ));
        $this->set_start($start);
        $this->set_stop($stop);
        $fieldv = explode(";", $field);
        $valuev = explode(";", $value);
        if (count($valuev) > 0) {
            for ($a = 0; $a < count($valuev); $a++) {
                if ($valuev[$a] !== '') {
                    $this->set_like(array(
                        $this->get_table() . '.'.$fieldv[$a] => $valuev[$a]
                    ));
                }
            }
        }
        $this->set_orderby('date_edit desc');
        return $this->get_data();
    }
        

}

/* End of file atribut_kriteria_model.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/sanmar/app/models/atribut_kriteria_model.php */